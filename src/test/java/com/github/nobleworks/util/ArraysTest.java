package com.github.nobleworks.util;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ArraysTest
{
	private final Object[] objectArray =
		{
			new Float( 2.5 ), null, new Object(),
			Integer.valueOf( 1 ), Boolean.FALSE
		};

	private final byte[] byteArray =
		{
			5, 10, -1, -128, 127
		};

	@Test public void testResizeObjectArraySmaller()
	{
		Object[] array1 = objectArray.clone();

		Object[] array2 = Arrays.resize( array1, 3 );

		assertThat( array1, not( sameInstance( array2 ) ) );
		assertThat( array2, notNullValue() );
		assertThat(array2.length, is(3));

		// Test that the array passed in is not modified
		for( int i = 0; i < objectArray.length; i++ )
		{
			assertSame( objectArray[ i ], array1[ i ] );
		}

		// Test that the output contains the same values
		for( int i = 0; i < array2.length; i++ )
		{
			assertSame( objectArray[ i ], array2[ i ] );
		}
	}

	@Test public void testResizeObjectArrayLarger()
	{
		final Object[] array1 = objectArray.clone();

		Object[] array2 = Arrays.resize( array1, 7 );

		assertThat( array1, not( sameInstance( array2 ) ) );
		assertThat( array2, notNullValue() );
		assertThat( array2.length, is( 7 ) );

		// Test that the array passed in is not modified
		for( int i = 0; i < objectArray.length; i++ )
		{
			assertSame( objectArray[ i ], array1[ i ] );
		}

		// Test that the output contains the same values
		for( int i = 0; i < array1.length; i++ )
		{
			assertSame( objectArray[ i ], array2[ i ] );
		}

		// Test that the added values are null
		for( int i = array1.length; i < array2.length; i++ )
		{
			assertThat( array2[ i ], nullValue() );
		}
	}

	@Test public void resizingObjectArrayToSameSizeShouldReturnSameArrayUnmodified()
	{
		Object[] array1 = objectArray.clone();

		Object[] array2 = Arrays.resize( array1, 5 );

		assertThat( array1, sameInstance( array2 ) );

		// Test that the array passed in is not modified
		for( int i = 0; i < objectArray.length; i++ )
		{
			assertSame( objectArray[ i ], array1[ i ] );
		}
	}

	@Test public void testResizeByteArraySmaller()
	{
		byte[] array1 = byteArray.clone();

		byte[] array2 = Arrays.resize( array1, 3 );

		assertThat( array1, not( sameInstance( array2 ) ) );
		assertNotNull( array2 );
		assertEquals( 3, array2.length );

		// Test that the array passed in is not modified
		for( int i = 0; i < byteArray.length; i++ )
		{
			assertSame( byteArray[ i ], array1[ i ] );
		}

		// Test that the output contains the same values
		for( int i = 0; i < array2.length; i++ )
		{
			assertSame( byteArray[ i ], array2[ i ] );
		}
	}

	@Test public void testResizeByteArrayLarger()
	{
		byte[] array1 = byteArray.clone();

		byte[] array2 = Arrays.resize( array1, 7 );

		assertThat( "Resized array", array2, not( sameInstance( array1 ) ) );
		assertThat( "Resized array", array2, notNullValue() );
		assertThat( "Resized array length", array2.length, is( 7 ) );

		// Test that the array passed in is not modified
		for( int i = 0; i < byteArray.length; i++ )
		{
			assertEquals(byteArray[i], array1[i]);
		}

		// Test that the output contains the same values
		for( int i = 0; i < array1.length; i++ )
		{
			assertEquals(byteArray[i], array2[i]);
		}

		// Test that the added values are null
		for( int i = array1.length; i < array2.length; i++ )
		{
			assertEquals(0, array2[i]);
		}
	}
}
