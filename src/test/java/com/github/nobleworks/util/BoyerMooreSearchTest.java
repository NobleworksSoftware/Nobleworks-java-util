package com.github.nobleworks.util;

import org.junit.Test;

import junit.framework.TestCase;

public class BoyerMooreSearchTest extends TestCase
{
    @Test public void testSearch()
    {
        String pattern = "bed";
        String target = "bedbeedbedbbeds";

        BoyerMooreSearch bms = new BoyerMooreSearch( pattern );

        assertEquals( 0, bms.search( target ) );
        assertEquals( 0, bms.search( target ) );
        assertEquals( 0, bms.search( target, 0 ) );

        assertEquals( 7, bms.search( target, 1 ) );
        assertEquals( 7, bms.search( target, 7 ) );

        assertEquals( 11, bms.search( target, 8 ) );
        assertEquals( 11, bms.search( target, 11 ) );

        assertEquals( -1, bms.search( target, 12 ) );
        assertEquals( -1, bms.search( target, target.length() ) );
        assertEquals( -1, bms.search( target, -1 ) );

        assertEquals( 0, bms.search( target ) );
        assertEquals( 0, bms.search( target, 0 ) );
    }
}
