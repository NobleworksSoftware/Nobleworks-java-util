package com.github.nobleworks.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.BitSet;
import java.util.Random;

import org.junit.Test;


/**
 * Test case for methods of Routines.
 * 
 * @author Dale King, Nobleworks Software
 */
public class RoutinesTest
{
	public static class CountBitsSetTest
	{
		@Test public void shouldReturn0ForZero()
		{
			assertThat( Routines.countBitsSet( 0 ), is( 0 ) );
		}

		@Test public void shouldReturn1ForOne()
		{
			assertThat( Routines.countBitsSet( 1 ), is( 1 ) );
		}

		@Test public void shouldReturn32ForNegativeOne()
		{
			assertThat(Routines.countBitsSet(-1), is(32));
		}

		@Test public void shouldReturn31ForIntegerMaxValue()
		{
			assertThat( Routines.countBitsSet( Integer.MAX_VALUE ), is( 31 ) );
		}

		@Test public void shouldReturn1ForIntegerMinValue()
		{
			assertThat( Routines.countBitsSet( Integer.MIN_VALUE ), is( 1 ) );
		}

		/**
		 * Perform tests of the countBitsSet routine for 1000 random values
		 * by randomly toggling one bit and then incrementing or decrementing
		 * the expected number of bits depending on whether the bit became set
		 * or cleared.
		 */
		@Test public void shouldReturnCorrectNumberOfBitsForOtherRandomValues()
		{
			Random r = new Random();

			int value = 0;
			int bits = 0;

			// try a bunch of random combinations by toggling a random bit
			for( int i = 0; i < 1000; i++ )
			{
				// Generate a mask for a random bit.
				final int mask = 1 << r.nextInt( Integer.SIZE );

				// See if the bit is already set
				if( ( mask & value ) == 0 )
				{
					// If the bit is not already set we will
					// increase the number of bits
					bits++;
				}
				else
				{
					// If the bit is already set we will
					// decrease the number of bits
					bits--;
				}

				value ^= mask;

				String format = "countBitsSet( 0x%X )"; //$NON-NLS-1$

				assertThat( String.format( format, value ),
						Routines.countBitsSet( value ), is( bits ) );
			}
		}
	}

	public static class CountBitsSetLongTest
	{
		@Test public void shouldReturn0ForZero()
		{
			assertThat( Routines.countBitsSet( 0L ), is( 0 ) );
		}

		@Test public void shouldReturn1ForOne()
		{
			assertThat( Routines.countBitsSet( 1L ), is( 1 ) );
		}

		@Test public void shouldReturn64ForNegativeOne()
		{
			assertThat( Routines.countBitsSet( -1L ), is( 64 ) );
		}

		@Test public void shouldReturn63ForLongMaxValue()
		{
			assertThat( Routines.countBitsSet( Long.MAX_VALUE ), is( 63 ) );
		}

		@Test public void shouldReturn1ForLongMinValue()
		{
			assertThat( Routines.countBitsSet( Long.MIN_VALUE ), is( 1 ) );
		}

		/**
		 * Perform tests of the countBitsSet routine for 1000 random values
		 * by randomly toggling one bit and then incrementing or decrementing
		 * the expected number of bits depending on whether the bit became set
		 * or cleared.
		 */
		@Test public void shouldReturnCorrectNumberOfBitsForOtherRandomValues()
		{
			final Random rand = new Random();

			long value = 0;
			int bits = 0;

			// try a bunch of random combinations by toggling a random bit
			for( int i = 0; i < 1000; i++ )
			{
				// Generate a mask for a random bit.
				final long mask = 1L << rand.nextInt( Long.SIZE );

				// See if the bit is already set
				if( ( mask & value ) == 0 )
				{
					// If the bit is not already set we will
					// increase the number of bits
					bits++;
				}
				else
				{
					// If the bit is already set we will
					// decrease the number of bits
					bits--;
				}

				value ^= mask;

				String format = "countBitsSet( 0x%XL )"; //$NON-NLS-1$

				assertThat( String.format( format, value ),
						Routines.countBitsSet( value ), is( bits ) );
			}
		}
	}

	public static class ShuffleTest
	{
		/**
		 * Tests the array shuffling routine. It simply creates two arrays one of
		 * ints the other of Integers and tests that after running shuffle on them
		 * that they contain all the same values and that they are not in the same
		 * order.
		 *
		 * Theoretically this test can fail because it is possible that the shuffle
		 * might not change the order. The odds of this happening however are only
		 * 2 / 1024!
		 */
		@Test public void testShuffle()
		{
			final int arrayLength = 1024;
			int[] intArray = new int[ arrayLength ];
			Object[] integerArray = new Object[ arrayLength ];

			for( int i = 0; i < integerArray.length; i++ )
			{
				intArray[ i ] = i;
				integerArray[ i ] = -i;
			}

			Object[] integerArrayCopy = integerArray.clone();

			Routines.shuffle( intArray );
			Routines.shuffle( integerArray );

			boolean isShuffled = false;

			for( int i = 0; i < integerArray.length; i++ )
			{
				Integer integer = (Integer) integerArray[ i ];

				int index = -integer.intValue();

				if( index != i )
				{
					isShuffled = true;
				}

				Object copy = integerArrayCopy[ index ];

				assertNotNull( "Array Element Duplicated", copy ); //$NON-NLS-1$
				assertTrue( "Array element was changed", copy == integer ); //$NON-NLS-1$

				integerArrayCopy[ index ] = null;
			}

			assertTrue( "The array was not changed. Note that there is\n" + //$NON-NLS-1$
					"an infantessimally small probability that this\n" + //$NON-NLS-1$
					"test can fail, so try rerunning if it does.", isShuffled ); //$NON-NLS-1$

			BitSet intSet = new BitSet();

			isShuffled = false;

			for( int i = 0; i < intArray.length; i++ )
			{
				int value = intArray[ i ];

				if( value != i )
				{
					isShuffled = true;
				}

				assertTrue( "Negative value introduced into array", //$NON-NLS-1$
						value >= 0 );
				assertTrue( "New value introduced into array", //$NON-NLS-1$
						value < arrayLength );
				assertFalse( "Value duplicated in array", //$NON-NLS-1$
						intSet.get( value ) );

				intSet.set( value );
			}

			assertTrue( "The array was not changed. Note that there is\n" + //$NON-NLS-1$
					"an infantessimally small probability that this\n" + //$NON-NLS-1$
					"test can fail, so try rerunning if it does.", isShuffled ); //$NON-NLS-1$
		}

		@Test( expected=NullPointerException.class )
		public void shouldThrowNullPointerExceptionWhenNullPassed()
		{
			Routines.shuffle( null );
		}

		@Test( expected=IllegalArgumentException.class )
		public void shouldThrowIllegalArgumentExceptionWhenNonArrayPassed()
		{
			Routines.shuffle( new Object() );
		}

		/**
		 * Tests the array shuffling routine for some border cases. Tests that
		 * <ul>
		 *   <li>Properly handles a zero length array.
		 *   <li>Properly handles a 1 element array.
		 * </ul>
		 */
		@Test public void testShuffleBorderCases()
		{
			float[] emptyArray = new float[ 0 ];
			char[] length1Array =
				{
					'@'
				};

			Routines.shuffle( emptyArray );
			Routines.shuffle( length1Array );

			assertEquals( "Length 1 array should not be changed", //$NON-NLS-1$
					'@', length1Array[ 0 ] );
		}
	}
}
