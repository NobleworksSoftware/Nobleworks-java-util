package com.github.nobleworks.utility.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import junit.framework.TestCase;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;

import org.junit.Before;
import org.junit.Test;

public class ReadOnlyListSelectionModelTest
{
	@Mocked
	private ListSelectionModel mockModel;

	/**
	 * The actual instance of ReadOnlyListSelectionModel used for testing.
	 */
	private ListSelectionModel readOnlyModel;

	@Mocked
	private ListSelectionListener mockListener;

	/**
	 * Random instance for generating random input data.
	 */
	private final Random r = new Random();

	/**
	 * Random integers to be used in testing.
	 */
	private int randomInt1, randomInt2;

	/**
	 * Sets up the mock objects and the instance we are actually testing.
	 * @see TestCase#setUp()
	 */
	@Before
	public void setUp()
	{
		readOnlyModel = new ReadOnlyListSelectionModel(mockModel);

		randomInt1 = r.nextInt();
		randomInt2 = r.nextInt();
	}

	/**
	 * Test that passing a null to the constructor generates a
	 * NullPointerException.
	 */
	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionWhenNullPassedToConstructor()
	{
		new ReadOnlyListSelectionModel(null);
	}

	/**
	 * Invokes all the methods on the object that are not supposed to do
	 * anything.
	 */
	public void invokeDoNothingMethods()
	{
		readOnlyModel.setSelectionInterval(r.nextInt(), r.nextInt());
		readOnlyModel.addSelectionInterval( r.nextInt(), r.nextInt() );
		readOnlyModel.removeSelectionInterval( r.nextInt(), r.nextInt() );
		readOnlyModel.setAnchorSelectionIndex( r.nextInt() );
		readOnlyModel.setLeadSelectionIndex( r.nextInt() );
		readOnlyModel.clearSelection();
		readOnlyModel.insertIndexInterval( r.nextInt(), r.nextInt(), true );
		readOnlyModel.insertIndexInterval( r.nextInt(), r.nextInt(), false );
		readOnlyModel.removeIndexInterval( r.nextInt(), r.nextInt() );
		readOnlyModel.setValueIsAdjusting( true );
		readOnlyModel.setValueIsAdjusting( false );
		readOnlyModel.setSelectionMode( r.nextInt() );
	}

	/**
	 * This tests all of the methods that are supposed to do nothing. If any
	 * of these call any method on the wrapped object that will be caught in
	 * the mock object.
	 */
	@Test
	public void testDoNothingMethods()
	{
		invokeDoNothingMethods();

		// Verify nothing got called on the wrapped model
		new FullVerifications(mockModel) {
		};
	}

	/**
	 * Tests that the toString method is passed through to the underlying
	 * wrapped object.
	 */
	@Test
	public void testToString()
	{
		assertEquals(mockModel.toString(), readOnlyModel.toString());
	}

	/**
	 * Tests that the getMinSelectionIndex method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testGetMinSelectionIndex()
	{
		new Expectations() {
			{
				mockModel.getMinSelectionIndex();
				returns(randomInt1, randomInt2);
			}
		};

		assertEquals(randomInt1, readOnlyModel.getMinSelectionIndex());
		assertEquals(randomInt2, readOnlyModel.getMinSelectionIndex());
	}

	/**
	 * Tests that the getMaxSelectionIndex method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testGetMaxSelectionIndex()
	{
		new Expectations() {
			{
				mockModel.getMaxSelectionIndex();
				returns(randomInt1, randomInt2);
			}
		};

		assertEquals(randomInt1, readOnlyModel.getMaxSelectionIndex());
		assertEquals(randomInt2, readOnlyModel.getMaxSelectionIndex());
	}

	/**
	 * Tests that the getAnchorSelectionIndex method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testGetAnchorSelectionIndex()
	{
		new Expectations() {
			{
				mockModel.getAnchorSelectionIndex();
				returns(randomInt1, randomInt2);
			}
		};

		assertEquals(randomInt1, readOnlyModel.getAnchorSelectionIndex());
		assertEquals(randomInt2, readOnlyModel.getAnchorSelectionIndex());
	}

	/**
	 * Tests that the getLeadSelectionIndex method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testGetLeadSelectionIndex()
	{
		new Expectations() {
			{
				mockModel.getLeadSelectionIndex();
				returns(randomInt1, randomInt2);
			}
		};

		assertEquals(randomInt1, readOnlyModel.getLeadSelectionIndex());
		assertEquals(randomInt2, readOnlyModel.getLeadSelectionIndex());
	}

	/**
	 * Tests that the getValueIsAdjusting method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testGetValueIsAdjusting()
	{
		new Expectations() {
			{
				mockModel.getValueIsAdjusting();
				returns(false, true);
			}
		};

		assertFalse(readOnlyModel.getValueIsAdjusting());
		assertTrue(readOnlyModel.getValueIsAdjusting());
	}

	/**
	 * Tests that the isSelectionEmpty method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testIsSelectionEmpty()
	{
		new Expectations() {
			{
				mockModel.isSelectionEmpty();
				returns(false, true);
			}
		};

		assertFalse(readOnlyModel.isSelectionEmpty());
		assertTrue(readOnlyModel.isSelectionEmpty());
	}

	/**
	 * Tests that the getSelectionMode method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testGetSelectionMode()
	{
		new Expectations() {
			{
				mockModel.getSelectionMode();
				returns(randomInt1, randomInt2);
			}
		};

		assertEquals(randomInt1, readOnlyModel.getSelectionMode());
		assertEquals(randomInt2, readOnlyModel.getSelectionMode());
	}

	/**
	 * Tests that the isSelectedIndex method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testIsSelectedIndex()
	{
		new Expectations() {
			{
				mockModel.isSelectedIndex(randomInt1);
				returns(false);

				mockModel.isSelectedIndex(randomInt2);
				returns(true);
			}
		};

		assertFalse(readOnlyModel.isSelectedIndex(randomInt1));
		assertTrue(readOnlyModel.isSelectedIndex(randomInt2));
	}

	/**
	 * Tests that the addListSelectionListener method is delegated to the
	 * wrapped model and that none of the methods that are supposed to do
	 * nothing make calls to the listener added.
	 */
	@Test
	public void testAddListSelectionListener()
	{
		new Expectations() {
			{
				mockModel
				.addListSelectionListener((ListSelectionListener) withNull());
				mockModel.addListSelectionListener(mockListener);
			}
		};

		readOnlyModel.addListSelectionListener( null );
		readOnlyModel.addListSelectionListener( mockListener );

		invokeDoNothingMethods();

		new FullVerifications(mockListener) {
		};
	}

	/**
	 * Tests that the isSelectedIndex method is delegated to the wrapped
	 * model.
	 */
	@Test
	public void testRemoveListSelectionListener()
	{
		new Expectations() {
			{
				mockModel
						.removeListSelectionListener((ListSelectionListener) withNull());
				mockModel.removeListSelectionListener(mockListener);
			}
		};

		readOnlyModel.removeListSelectionListener( null );
		readOnlyModel.removeListSelectionListener( mockListener );
	}
}
