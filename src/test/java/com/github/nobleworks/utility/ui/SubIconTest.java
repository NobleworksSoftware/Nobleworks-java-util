package com.github.nobleworks.utility.ui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.Icon;

import mockit.Expectations;
import mockit.Mocked;

import org.junit.Test;

import com.github.nobleworks.util.RandomData;
import com.github.nobleworks.util.RandomData.Generator;

public class SubIconTest
{
	private Generator< Rectangle > rectGenerator
	= RandomData.getRandomGenerator( Rectangle.class );

	@Mocked
	private Icon mockIcon;

	@Mocked
	private Icon mockIcon2;

	@Test
	public void testRectangleConstructor()
	{
		Rectangle r = rectGenerator.getValue();
		SubIcon icon = new SubIcon( mockIcon, r );

		Rectangle rectangle = icon.getRectangle();
		assertThat(rectangle, not(sameInstance(r)));
		assertThat(rectangle, equalTo(r));
		assertThat(icon.getIconHeight(), equalTo(r.height));
		assertThat(icon.getIconWidth(), equalTo(r.width));
		assertThat(icon.getSourceIcon(), sameInstance(mockIcon));
	}

	@Test
	public void testIntBoundsConstructor()
	{
		Rectangle r = rectGenerator.getValue();
		SubIcon icon = new SubIcon( mockIcon, r.x, r.y, r.width, r.height );

		Rectangle rectangle = icon.getRectangle();
		assertThat(rectangle, equalTo(r));
		assertThat(icon.getIconHeight(), equalTo(r.height));
		assertThat(icon.getIconWidth(), equalTo(r.width));
		assertThat(icon.getSourceIcon(), sameInstance(mockIcon));
	}

	@Test
	public void testSourceIcon()
	{
		Rectangle r = rectGenerator.getValue();
		SubIcon icon = new SubIcon( mockIcon, r );

		assertSame( mockIcon, icon.getSourceIcon() );

		icon.setSourceIcon( null );
		assertNull( icon.getSourceIcon() );

		icon.setSourceIcon( mockIcon2 );
		assertSame( mockIcon2, icon.getSourceIcon() );
	}

	@Test
	public void testSetRectangle()
	{
		Rectangle r = rectGenerator.getValue();
		SubIcon icon = new SubIcon( mockIcon, r );

		Rectangle rectangle = icon.getRectangle();
		assertNotSame( r, rectangle );
		assertEquals( r, rectangle );
		assertEquals( r.height, icon.getIconHeight() );
		assertEquals( r.width, icon.getIconWidth() );

		r = rectGenerator.getValue();
		icon.setRectangle( r );

		rectangle = icon.getRectangle();
		assertNotSame( r, rectangle );
		assertEquals( r, rectangle );
		assertEquals( r.height, icon.getIconHeight() );
		assertEquals( r.width, icon.getIconWidth() );
	}

	@Test
	public void testSetBounds()
	{
		Rectangle r = rectGenerator.getValue();
		SubIcon icon = new SubIcon( mockIcon, r.x, r.y, r.width, r.height );

		Rectangle rectangle = icon.getRectangle();
		assertNotSame( r, rectangle );
		assertEquals( r, rectangle );
		assertEquals( r.height, icon.getIconHeight() );
		assertEquals( r.width, icon.getIconWidth() );

		r = rectGenerator.getValue();
		icon.setBounds( r.x, r.y, r.width, r.height );

		rectangle = icon.getRectangle();
		assertNotSame( r, rectangle );
		assertEquals( r, rectangle );
		assertEquals( r.height, icon.getIconHeight() );
		assertEquals( r.width, icon.getIconWidth() );
	}

	@Test
	public void testPaintIcon(@Mocked final Graphics g, @Mocked final Graphics clipped,
			@Mocked final Component c)
	{
		final Rectangle r = rectGenerator.getValue();
		final SubIcon icon = new SubIcon( mockIcon, r.x, r.y, r.width, r.height );

		final int[] coordinates = RandomData.ArrayGenerator.INT.getValue(4);

		new Expectations() {
			{
				for (int i = 0; i < coordinates.length; i += 2) {
					g.create(coordinates[i], coordinates[i + 1],
							r.width, r.height);
					result = clipped;

					mockIcon.paintIcon(c, clipped, -r.x, -r.y);

					clipped.dispose();
				}
			}
		};

		for( int i = 0; i < coordinates.length; i += 2 )
		{
			icon.paintIcon( c, g, coordinates[ i ], coordinates[ i + 1 ] );
		}
	}
}
