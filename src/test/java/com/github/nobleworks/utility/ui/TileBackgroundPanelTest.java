package com.github.nobleworks.utility.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.Border;

import junit.framework.AssertionFailedError;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;
import mockit.VerificationsInOrder;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for TileBackgroundPanel.
 * 
 * @author Dale King, Nobleworks Software
 */
public class TileBackgroundPanelTest
{
    private TileBackgroundPanel paintingPanel;

    @Mocked
    private Graphics g;
    @Mocked
    private Graphics gCreated;
    @Mocked
    private Icon mockIcon;

    private static final int HEIGHT = 10;
    private static final int WIDTH = 5;

    @SuppressWarnings("serial")
    @Before
    public void setUp()
    {
        paintingPanel = new TileBackgroundPanel()
        {
            // Skip all the swing stuff and just do the only
            // call we are concerned with.
            @Override
            public void paint(Graphics gfx)
            {
                paintComponent(gfx);
            }
        };

        paintingPanel.setSize(WIDTH, HEIGHT);

        // This makes sure that we don't have to worry about the
        // calls in the superclass' paintComponent method.
        paintingPanel.setUI(null);
    }

    @Test
    public void testDefaultConstructor()
    {
        TileBackgroundPanel p = new TileBackgroundPanel();

        assertEquals(true, p.isDoubleBuffered());
        assertTrue(p.getLayout() instanceof FlowLayout);
        assertNull(p.getIcon());
    }

    @Test
    public void testBooleanConstructor()
    {
        TileBackgroundPanel p = new TileBackgroundPanel(true);

        assertEquals("isDoubleBuffered", //$NON-NLS-1$
                true, p.isDoubleBuffered());
        assertTrue("layout is flow layout", //$NON-NLS-1$
                p.getLayout() instanceof FlowLayout);
        assertNull("tile icon", p.getIcon()); //$NON-NLS-1$

        p = new TileBackgroundPanel(false);

        assertEquals("isDoubleBuffered", //$NON-NLS-1$
                false, p.isDoubleBuffered());
        assertTrue("layout is flow layout", //$NON-NLS-1$
                p.getLayout() instanceof FlowLayout);
        assertNull("tile icon", p.getIcon()); //$NON-NLS-1$
    }

    @Test
    public void testLayoutManagerConstructor()
    {
        LayoutManager m = new BorderLayout();
        TileBackgroundPanel p = new TileBackgroundPanel(m);

        assertEquals("isDoubleBuffered", //$NON-NLS-1$
                true, p.isDoubleBuffered());
        assertSame("layout manager", m, p.getLayout()); //$NON-NLS-1$
        assertNull("tile icon", p.getIcon()); //$NON-NLS-1$
    }

    @Test
    public void testLayoutManagerBooleanConstructor()
    {
        LayoutManager m = new BorderLayout();
        TileBackgroundPanel p = new TileBackgroundPanel(m, true);

        assertEquals("isDoubleBuffered", //$NON-NLS-1$
                true, p.isDoubleBuffered());
        assertSame("layout manager", m, p.getLayout()); //$NON-NLS-1$
        assertNull("tile icon", p.getIcon()); //$NON-NLS-1$

        m = new GridBagLayout();
        p = new TileBackgroundPanel(m, false);

        assertEquals("isDoubleBuffered", //$NON-NLS-1$
                false, p.isDoubleBuffered());
        assertSame("layout manager", m, p.getLayout()); //$NON-NLS-1$
        assertNull("tile icon", p.getIcon()); //$NON-NLS-1$
    }

    @Test
    public void testIconProperty(@Mocked("repaint") final TileBackgroundPanel p)
    {
        new Expectations()
        {
            {
                p.repaint();
                // Each call to setIcon, triggers a repaint
                times = 2;
            }
        };
        p.setIcon(null);
        assertNull(p.getIcon());

        Icon i = new ImageIcon();
        p.setIcon(i);
        assertSame(i, p.getIcon());

        new FullVerifications(p)
        {
        };
    }

    /**
     * Test that painting does nothing if the icon is null.
     */

    @Test
    public void testPaintingWithNullIcon()
    {
        paintingPanel.setIcon(null);

        doPaint();
    }

    private void doPaint()
    {
        paintingPanel.paint(g);
    }

    private void testIconPaint(final int h, final int w) throws AssertionFailedError
    {
        new NonStrictExpectations()
        {
            {
                mockIcon.getIconHeight();
                returns(h);

                mockIcon.getIconWidth();
                returns(w);
            }
        };

        paintingPanel.setIcon(mockIcon);

        doPaint();
    }

    /**
     * Test that painting does nothing if the icon height and width are less
     * than 0.
     */
    @Test
    public void testPaintingWithNegativeHeightAndWidth()
    {
        testIconPaint(-1, -1);
    }

    /**
     * Test that painting does nothing if the icon height is zero and width is
     * less than 0.
     */
    @Test
    public void testPaintingWithZeroHeightAndNegativeWidth()
    {
        testIconPaint(0, -1);
    }

    /**
     * Test that painting does nothing if the icon height is positive and width
     * is less than 0.
     */
    @Test
    public void testPaintingWithPositiveHeightAndNegativeWidth()
    {
        testIconPaint(1, -1);
    }

    /**
     * Test that painting does nothing if the icon width is zero and height is
     * less than 0.
     */
    @Test
    public void testPaintingWithZeroWidthAndNegativeHeight()
    {
        testIconPaint(-1, 0);
    }

    /**
     * Test that painting does nothing if the icon width is positive and height
     * is less than 0.
     */
    @Test
    public void testPaintingWithNegativeHeightAndPositiveWidth()
    {
        testIconPaint(-1, 1);
    }

    /**
     * Test that painting does nothing if the icon width is positive and height
     * is less than 0.
     */
    @Test
    public void testPaintingWithZeroHeightAndPositiveWidth()
    {
        testIconPaint(0, 1);
    }

    /**
     * Test that painting does nothing if the icon width is zero and height is
     * positive.
     */
    @Test
    public void testPaintingWithZeroWidthAndPositiveHeight()
    {
        testIconPaint(1, 0);
    }

    /**
     * Test that painting does nothing if the icon width and height are 0.
     */
    @Test
    public void testPaintingWithZeroHeightAndWidth()
    {
        testIconPaint(0, 0);
    }

    @Test
    public void testPaintingIcon1x1()
    {
        testPainting(1, 1);
    }

    private void testPainting(final int iconWidth, final int iconHeight) throws AssertionFailedError
    {
        Insets insets = new Insets(0, 0, 0, 0);
        testPaintingWithInset(iconWidth, iconHeight, insets);
    }

    private void testPaintingWithInset(final int iconWidth, final int iconHeight, Insets insets)
            throws AssertionFailedError
            {
        final Rectangle r = new Rectangle(0, 0, WIDTH, HEIGHT);

        // Why is there no method to do this?
        r.x += insets.left;
        r.width -= insets.right + insets.left;
        r.y += insets.top;
        r.height -= insets.top + insets.bottom;

        new NonStrictExpectations()
        {
            {
                mockIcon.getIconHeight();
                returns(iconHeight);
                mockIcon.getIconWidth();
                returns(iconWidth);

                g.create(withEqual(r.x), withEqual(r.y), withEqual(r.width), withEqual(r.height));
                returns(gCreated);
                times = 1;

                for (int x = 0; x < r.width; x += iconWidth)
                {
                    for (int y = 0; y < r.height; y += iconHeight)
                    {
                        mockIcon.paintIcon(withSameInstance(paintingPanel), withSameInstance(gCreated), withEqual(x),
                                withEqual(y));
                        times = 1;
                    }
                }

                gCreated.dispose();
                times = 1;
            }
        };

        testIconPaint(iconHeight, iconWidth);

        // These calls can happen at any time
        new Verifications()
        {
            {
                mockIcon.getIconHeight();
                mockIcon.getIconWidth();
            }
        };

        new VerificationsInOrder()
        {
            {
                g.create(withEqual(r.x), withEqual(r.y), withEqual(r.width), withEqual(r.height));
                unverifiedInvocations();
                gCreated.dispose();
            }
        };
            }

    @Test
    public void testPaintingIcon2x3()
    {
        testPainting(2, 3);
    }

    @Test
    public void testPaintingIcon2x3WithBorder()
    {
        Insets insets = new Insets(2, 1, 4, 3);

        Border border = BorderFactory.createEmptyBorder(insets.top, insets.left, insets.bottom, insets.right);

        paintingPanel.setBorder(border);

        testPaintingWithInset(2, 3, insets);
    }
}
