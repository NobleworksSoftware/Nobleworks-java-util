package com.github.nobleworks.utility.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.event.ActionEvent;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import javax.swing.Action;
import javax.swing.event.ChangeEvent;

import org.junit.Test;

import com.github.nobleworks.util.RandomData;

public class AbstractChangeListenerActionTest
{
	private RandomData.Generator< String > stringGenerator
	= RandomData.getRandomGenerator( String.class );

	@SuppressWarnings("serial")
	@Test
	public void testChangeListenerActionConstructor() throws SecurityException,
	NoSuchMethodException {
		String name = stringGenerator.getValue().toString();

		Class< ? > theClass = AbstractChangeListenerAction.class;

		assertTrue(Action.class.isAssignableFrom(theClass));

		Method m = theClass.getMethod( "actionPerformed", //$NON-NLS-1$
				new Class[] { ActionEvent.class } );

		assertTrue( "actionPerformed is abstract", //$NON-NLS-1$
				Modifier.isAbstract( m.getModifiers() ) );

		AbstractChangeListenerAction a = new AbstractChangeListenerAction(name) {
			public void actionPerformed(ActionEvent arg0) {
				fail("actionPerformed was called"); //$NON-NLS-1$
			}
		};

		assertEquals("Name passed to action constructor", name,
				a.getValue("Name"));
	}

	@SuppressWarnings("serial")
	@Test public void testStateChanged()
	{
		AbstractChangeListenerAction a = new AbstractChangeListenerAction("foo")
		{
			public void actionPerformed( ActionEvent arg0 )
			{
				fail( "actionPerformed was called" ); //$NON-NLS-1$
			}
		};

		a.stateChanged( new ChangeEvent( this ) );
	}
}
