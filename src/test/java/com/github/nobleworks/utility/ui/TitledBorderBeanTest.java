package com.github.nobleworks.utility.ui;

import static org.junit.Assert.assertNull;

import javax.swing.border.TitledBorder;

import org.junit.Test;

public class TitledBorderBeanTest
{
	@Test
	public void testConstructor()
	{
		TitledBorder tb = new TitledBorderBean();
		assertNull( tb.getTitle() );
	}
}
