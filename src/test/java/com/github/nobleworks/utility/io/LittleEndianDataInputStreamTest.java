package com.github.nobleworks.utility.io;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import mockit.Delegate;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.github.nobleworks.util.RandomData;
import com.github.nobleworks.util.Routines;

/**
 * Test case for methods of LittleEndianDataInputStream</code>.
 * 
 * @author Dale King, Nobleworks Software
 */
public class LittleEndianDataInputStreamTest
{
    private final int[] data = new int[Byte.MAX_VALUE - Byte.MIN_VALUE];
    @Mocked
    InputStream input;
    private LittleEndianDataInputStream dis;

    @Before
    public void setup()
    {
        dis = new LittleEndianDataInputStream(input);
    }

    /**
     * Constructor for the test case.
     */
    @Before
    public void setupData()
    {
        for (int i = 0; i < data.length; i++)
        {
            data[i] = i;
        }

        Routines.shuffle(data);
    }

    @Test
    public void testReadBoolean() throws IOException
    {
        new Expectations()
        {
            {
                for (int i : data)
                {
                    input.read();
                    returns(i);
                }

                input.read();
                returns(-1);
            }
        };

        for (int i : data)
        {
            assertThat("readBoolean for byte " + i, dis.readBoolean(), is(i != 0));
        }

        try
        {
            dis.readBoolean();

            fail("Expected EOFException");
        }
        catch (EOFException e)
        {
        }
    }

    @Test
    public void testReadByte() throws IOException
    {
        new Expectations()
        {
            {
                input.read();

                for (int i : data)
                {
                    returns(i);
                }

                returns(-1);
            }
        };

        for (int i : data)
        {
            assertEquals("readByte of " + i, (byte) i, dis.readByte());
        }

        try
        {
            dis.readByte();

            fail("Expected EOFException");
        }
        catch (EOFException e)
        {
        }
    }

    @Test
    public void testReadUnsignedByte() throws IOException
    {
        new Expectations()
        {
            {
                input.read();

                for (int i : data)
                {
                    returns(i);
                }

                returns(-1);
            }
        };

        for (int i : data)
        {
            assertEquals("readUnsignedByte of " + i, i, dis.readUnsignedByte());
        }

        try
        {
            dis.readUnsignedByte();

            fail("Expected EOFException");
        }
        catch (EOFException e)
        {
        }
    }

    @Test
    public void testReadShort() throws IOException
    {
        new Expectations()
        {
            {
                input.read();
                returns(0, 0xFF, 0xFF, 0);

                for (int i = 0; i < 255; i++)
                {
                    returns(i, i + 1);
                }

                returns(0xFF, 0xFF);
                returns(0xAA, -1, -1, 0x55);
            }
        };

        assertEquals("readShort of 00 FF", -256, //$NON-NLS-1$
                dis.readShort());

        assertEquals("readShort of FF 00", 255, //$NON-NLS-1$
                dis.readShort());

        short expected = 0x100;
        for (int i = 0; i < 255; i++)
        {
            assertEquals("readShort", expected, dis.readShort());

            expected += 0x101;
        }

        assertEquals("readShort of FF FF", -1, dis.readShort()); //$NON-NLS-1$

        try
        {
            dis.readShort();

            fail("Expected EOFException"); //$NON-NLS-1$
        }
        catch (EOFException e)
        {
            // We expect to get the exception.
        }

        try
        {
            dis.readShort();

            fail("Expected EOFException"); //$NON-NLS-1$
        }
        catch (EOFException e)
        {
        }

        new FullVerifications(input)
        {
        };
    }

    @Test
    public void testReadUnsignedShort() throws IOException
    {
        new Expectations()
        {
            {
                input.read();
                returns(0, 0xFF, 0xFF, 0);

                for (int i = 0; i < 255; i++)
                {
                    returns(i, i + 1);
                }

                returns(0xFF, 0xFF);
                returns(0xA5, -1, -1, 0x5A);
            }
        };

        assertEquals("readUnsignedShort of 00 FF", 0xFF00, dis.readUnsignedShort());

        assertEquals("readUnignedShort of FF 00", 255, dis.readUnsignedShort());

        int expected = 0x100;

        for (int i = 0; i < 255; i++)
        {
            assertEquals("readShort", expected, dis.readUnsignedShort());

            expected += 0x101;
        }

        assertEquals("readShort of FF FF", 0xFFFF, //$NON-NLS-1$
                dis.readUnsignedShort());

        try
        {
            dis.readUnsignedShort();

            fail("Expected EOFException"); //$NON-NLS-1$
        }
        catch (EOFException e)
        {
        }

        try
        {
            dis.readUnsignedShort();

            fail("Expected EOFException"); //$NON-NLS-1$
        }
        catch (EOFException e)
        {
            // We expect to get the exception.
        }

        new FullVerifications(input)
        {
        };
    }

    @Test
    public void testReadInt() throws IOException
    {
        int expected = 0x03020100;

        new Expectations()
        {
            {
                input.read();
                for (int i = 0; i < 252; i++)
                {
                    for (int j = i; j < i + 4; j++)
                    {
                        returns(j);
                    }
                }
            }
        };

        for (int i = 0; i < 252; i++)
        {
            assertEquals("readInt", expected, dis.readInt());

            expected += 0x01010101;
        }

        new FullVerifications(input)
        {
        };
    }

    @Test(expected = EOFException.class)
    public void readIntShouldThrowEOFExceptionWhenFirstByteReadIsEOF() throws IOException
    {
        new Expectations()
        {
            {
                input.read();
                returns(-1, 0x55);
            }
        };

        dis.readInt();
    }

    @Test(expected = EOFException.class)
    public void readIntShouldThrowEOFExceptionWhenSecondByteReadIsEOF() throws IOException
    {
        new Expectations()
        {
            {
                input.read();
                returns(0xAA, -1);
            }
        };

        dis.readInt();
    }

    @Test(expected = EOFException.class)
    public void readIntShouldThrowEOFExceptionWhenThirdByteReadIsEOF() throws IOException
    {
        new Expectations()
        {
            {
                input.read();
                returns(0xAA, 0x5A, -1, 0xA5);
            }
        };

        dis.readInt();
    }

    @Test(expected = EOFException.class)
    public void readIntShouldThrowEOFExceptionWhenFourthByteReadIsEOF() throws IOException
    {
        new Expectations()
        {
            {
                input.read();
                returns(0x55, 0xAA, 0x5A, -1);
            }
        };

        dis.readInt();
    }

    final static int dataLength = 10;

    @SuppressWarnings("unused")
    public void testReadFully1Param(final int chunkSize) throws IOException
    {
        final byte[] bytes = RandomData.ArrayGenerator.BYTE.getValue(dataLength);
        final byte[] copy = bytes.clone();
        final byte[] destination = RandomData.ArrayGenerator.BYTE.getValue(dataLength);
        new Expectations()
        {
            {
                for (int j = 0; j < bytes.length; j += chunkSize)
                {
                    final int index = j;
                    input.read(withSameInstance(destination), withEqual(j), withEqual(dataLength - j));
                    result = new Delegate<Integer>()
                            {
                        int read(byte[] array, int offset, int length)
                        {
                            int count = Math.min(chunkSize, length);

                            System.arraycopy(bytes, index, array, offset, count);

                            return count;
                        }
                            };
                }
            }
        };

        dis.readFully(destination);

        assertTrue("byte array not changed", Arrays.equals(bytes, copy));
        assertTrue("data read", Arrays.equals(bytes, destination));
    }

    @Test
    public void testReadFully1ParamShouldReadFully() throws IOException
    {
        for (int i = 1; i < dataLength; i++)
        {
            testReadFully1Param(i);
        }
    }

    public void testReadFully3Param(final int chunkSize) throws IOException
    {
        final byte[] bytes = RandomData.ArrayGenerator.BYTE.getValue(dataLength);
        final byte[] copy = bytes.clone();
        final byte[] destination = RandomData.ArrayGenerator.BYTE.getValue(dataLength + 10);
        final int offset = 5;
        new Expectations()
        {
            {
                for (int j = 0; j < bytes.length; j += chunkSize)
                {
                    final int index = j;
                    input.read(withSameInstance(destination), withEqual(j + offset), withEqual(dataLength - j));
                    result = new Delegate<Integer>()
                            {
                        @SuppressWarnings("unused")
                        int read(byte[] array, int offset, int length)
                        {
                            int count = Math.min(chunkSize, length);

                            System.arraycopy(bytes, index, array, offset, count);

                            return count;
                        }
                            };
                }
            }
        };

        dis.readFully(destination, offset, dataLength);

        assertTrue("byte array not changed", Arrays.equals(bytes, copy));
        assertTrue("data read", Arrays.equals(bytes, Arrays.copyOfRange(destination, offset, offset + dataLength)));
    }

    @Test
    public void testReadFully3ParamShouldReadFully() throws IOException
    {
        for (int i = 1; i < dataLength; i++)
        {
            testReadFully1Param(i);
        }
    }

    @RunWith(Parameterized.class)
    public static class ReadFully1ParmEOFTests
    {
        final int length;

        @Mocked
        InputStream input;
        private LittleEndianDataInputStream dis;

        @Before
        public void setup()
        {
            dis = new LittleEndianDataInputStream(input);
        }

        public ReadFully1ParmEOFTests(int length)
        {
            this.length = length;
        }

        @Parameters(name = "read fully with EOF after {0} bytes read")
        public static Collection<Object[]> params()
        {
            ArrayList<Object[]> data = new ArrayList<Object[]>();

            for (int i = 0; i < dataLength; i += 1)
            {
                data.add(new Object[] { i });
            }

            return data;
        }

        @Test(expected = EOFException.class)
        public void testThrowsEOF() throws IOException
        {
            final byte[] destination = RandomData.ArrayGenerator.BYTE.getValue(dataLength);

            new Expectations()
            {
                {
                    for (int i = 0; i < length; i++)
                    {
                        input.read(withSameInstance(destination), withEqual(i), withEqual(dataLength - i));
                        returns(1);
                    }

                    input.read(withSameInstance(destination), withEqual(length), withEqual(dataLength - length));
                    returns(-1);
                }
            };

            dis.readFully(destination);
        }
    }

    @RunWith(Parameterized.class)
    public static class ReadFully3ParmEOFTests
    {
        final int length;

        @Mocked
        InputStream input;
        private LittleEndianDataInputStream dis;

        @Before
        public void setup()
        {
            dis = new LittleEndianDataInputStream(input);
        }

        public ReadFully3ParmEOFTests(int length)
        {
            this.length = length;
        }

        @Parameters(name = "read fully with EOF after {0} bytes read")
        public static Collection<Object[]> params()
        {
            ArrayList<Object[]> data = new ArrayList<Object[]>();

            for (int i = 0; i < dataLength; i += 1)
            {
                data.add(new Object[] { i });
            }

            return data;
        }

        @Test(expected = EOFException.class)
        public void testThrowsEOF() throws IOException
        {
            final byte[] destination = RandomData.ArrayGenerator.BYTE.getValue(dataLength + 10);
            final int offset = 5;

            new Expectations()
            {
                {
                    for (int i = 0; i < length; i++)
                    {
                        input.read(withSameInstance(destination), withEqual(offset + i), withEqual(dataLength - i));
                        returns(1);
                    }

                    input.read(withSameInstance(destination), withEqual(length + offset),
                            withEqual(dataLength - length));
                    returns(-1);
                }
            };

            dis.readFully(destination, offset, dataLength);
        }
    }
}
