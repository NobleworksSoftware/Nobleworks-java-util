package com.github.nobleworks.utility.io;

import java.io.IOException;
import java.io.OutputStream;

import mockit.Expectations;
import mockit.Mocked;

import org.junit.Before;
import org.junit.Test;

import com.github.nobleworks.util.Routines;

/**
 * Test case for methods of LittleEndianDataInputStream</code>.
 * 
 * @author Dale King, Nobleworks Software
 */
public class LittleEndianDataOutputStreamTest
{
    @Mocked
    OutputStream output;

    private LittleEndianDataOutputStream classUnderTest;

    @Before public void setUp()
    {
        classUnderTest = new LittleEndianDataOutputStream( output );
    }

    /**
     * Test the construction of a <code>LittleEndianDataOutputStream</code>
     * when that constructor is passed a null for the wrapped output stream.
     * This tests that it will throw an <code>IllegalArgumentException</code>
     */
    @SuppressWarnings("resource")
    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullPassedToConstructor()
    {
        new LittleEndianDataOutputStream( null );
    }

    @Test public void shouldWrite0ForWriteBooleanFalse() throws IOException
    {
        new Expectations() {
            {
                output.write(0);
            }
        };

        classUnderTest.writeBoolean( false );
    }

    @Test public void shouldWrite1ForWriteBooleanTrue() throws IOException
    {
        new Expectations() {
            {
                output.write(1);
            }
        };

        classUnderTest.writeBoolean(true);
    }

    @Test public void testWriteBoolean() throws IOException
    {
        final boolean[] sequence = { true, false, true, true, false, false, true };

        new Expectations() {
            {
                for (boolean b : sequence) {
                    output.write(b ? 1 : 0);
                }
            }
        };

        for( boolean b : sequence )
        {
            classUnderTest.writeBoolean( b );
        }
    }

    @Test public void testWriteByte() throws IOException
    {
        final byte[] data = new byte[256];

        for( int i = 0; i < data.length; i++ )
        {
            data[ i ] = (byte) i;
        }

        Routines.shuffle( data );

        new Expectations() {
            {
                for (byte b : data) {
                    output.write(b);
                }
            }
        };

        for( byte b : data )
        {
            classUnderTest.writeByte( b );
        }
    }

    @Test public void testWriteShort() throws IOException
    {
        new Expectations() {
            {
                output.write(0);
                output.write(0xFF);
                output.write(0xFF);
                output.write(0);
            }
        };

        classUnderTest.writeShort(-256);
        classUnderTest.writeShort(255);
    }
}
