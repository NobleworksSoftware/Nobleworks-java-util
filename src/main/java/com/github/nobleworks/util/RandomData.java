package com.github.nobleworks.util;

import static com.github.nobleworks.util.Messages.getString;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class for generating random data for use in testing.
 * 
 * @author Dale King, Nobleworks Software.
 */
public final class RandomData
{
	/**
	 * Private constructor since all methods static.
	 */
	private RandomData()
	{
	}
	/**
	 * The source of random numbers.
	 */
	private static final Random RAND = new Random();

	/**
	 * The maximum size for random arrays.
	 */
	private static final int MAXIMUM_ARRAY_SIZE = 25;

	/**
	 * Interface defining a generator of random objects.
	 * @author Dale King, Nobleworks Software
	 * @param <T> The type of objects returned.
	 */
	public interface Generator< T >
	{
		/**
		 * Returns a new randomly generated value.
		 * @return A new random object.
		 */
		T getValue();

		/**
		 * Generator of random <code>Boolean</code> objects.
		 */
		Generator< Boolean > BOOLEAN = new Generator< Boolean >()
				{
			public Boolean getValue()
			{
				return Boolean.valueOf( RAND.nextBoolean() );
			}
				};

				/**
				 * Generator of random <code>Byte</code> objects.
				 */
				Generator< Byte > BYTE = new Generator< Byte >()
						{
					public Byte getValue()
					{
						return Byte.valueOf( (byte) RAND.nextInt( 2 << Byte.SIZE ) );
					}
						};

						Generator< Short > SHORT = new Generator< Short >()
								{
							public Short getValue()
							{
								return Short.valueOf( (short) RAND.nextInt( 2 << Short.SIZE ) );
							}
								};

								Generator< Character > CHARACTER = new Generator< Character >()
										{
									public Character getValue()
									{
										return Character.valueOf( (char) RAND.nextInt( 2 << Character.SIZE ) );
									}
										};

										Generator< Integer > INTEGER = new Generator< Integer >()
												{
											public Integer getValue()
											{
												return Integer.valueOf( RAND.nextInt() );
											}
												};

												Generator< Long > LONG = new Generator< Long >()
														{
													public Long getValue()
													{
														return Long.valueOf( RAND.nextLong() );
													}
														};

														Generator< Float > FLOAT = new Generator< Float >()
																{
															public Float getValue()
															{
																return Float.valueOf( RAND.nextFloat() );
															}
																};

																Generator< Double > DOUBLE = new Generator< Double >()
																		{
																	public Double getValue()
																	{
																		return Double.valueOf( RAND.nextDouble() );
																	}
																		};

																		/**
																		 * Generator of random object instances.
																		 */
																		Generator< Object > OBJECT = new Generator< Object >()
																				{
																			public Object getValue()
																			{
																				return new Object();
																			}
																				};

																				/**
																				 * Generator of random strings.
																				 */
																				Generator< String > STRING = new Generator< String >()
																						{
																					public String getValue()
																					{
																						return new String( ArrayGenerator.CHAR.getValue( 100 ) );
																					}
																						};

																						/**
																						 * Generator of random color values.
																						 */
																						Generator< Color > COLOR = new Generator< Color >()
																								{
																							public Color getValue()
																							{
																								return new Color( RAND.nextFloat(), RAND.nextFloat(), RAND.nextFloat() );
																							}
																								};

																								/**
																								 * Generator of random <code>AffineTransform</code> instances.
																								 */
																								Generator< AffineTransform > AFFINE_TRANSFORM
																								= new Generator< AffineTransform >()
																								{
																									public AffineTransform getValue()
																									{
																										return new AffineTransform( RAND.nextFloat(), RAND.nextFloat(),
																												RAND.nextFloat(), RAND.nextFloat(),
																												RAND.nextFloat(), RAND.nextFloat() );
																									}
																								};

																								Generator< Rectangle > RECTANGLE = new Generator< Rectangle >()
																										{
																									public Rectangle getValue()
																									{
																										return new Rectangle( RAND.nextInt(), RAND.nextInt(),
																												RAND.nextInt(), RAND.nextInt() );
																									}
																										};

																										Generator< Font > FONT = new Generator< Font >()
																												{
																											public Font getValue()
																											{
																												GraphicsEnvironment env
																												= GraphicsEnvironment.getLocalGraphicsEnvironment();

																												String[] families = env.getAvailableFontFamilyNames();

																												String family = families[ RAND.nextInt( families.length ) ];

																												return new Font( family, RAND.nextInt( 4 ), RAND.nextInt( 30 ) + 6 );
																											}
																												};

																												Generator< FontMetrics > FONT_METRICS = new Generator< FontMetrics >()
																														{
																													@Deprecated
																													public FontMetrics getValue()
																													{
																														return Toolkit.getDefaultToolkit()
																																.getFontMetrics( Generator.FONT.getValue() );
																													}
																														};

																														Generator< RenderingHints.Key > RENDERING_HINTS_KEY
																														= new Generator< RenderingHints.Key >()
																														{
																															public RenderingHints.Key getValue()
																															{
																																List< RenderingHints.Key > keys = new ArrayList< RenderingHints.Key >();

																																for( Field field : RenderingHints.class.getFields() )
																																{
																																	try
																																	{
																																		if( field.getType() == RenderingHints.Key.class )
																																		{
																																			keys.add( (RenderingHints.Key) field.get( null ) );
																																		}
																																	}
																																	catch( IllegalAccessException e )
																																	{
																																		// Should not happen
																																		assert false : "Got unexpected IllegalAccessException";
																																	}
																																}

																																return keys.get( RandomData.RAND.nextInt( keys.size() ) );
																															}
																														};

																														Generator< RenderingHints > RENDERING_HINTS = new Generator< RenderingHints >()
																																{
																															public RenderingHints getValue()
																															{
																																Map<RenderingHints.Key, List<Object>> keyToValueSet
																																= getRenderingHintsMap();

																																Map< RenderingHints.Key, Object> finalMap
																																= new HashMap< RenderingHints.Key, Object>();

																																for( Map.Entry< RenderingHints.Key, List< Object > > entry
																																		: keyToValueSet.entrySet() )
																																{
																																	List<Object> l = entry.getValue();

																																	finalMap.put( entry.getKey(), l.get( RAND.nextInt( l.size() ) ) );
																																}

																																return new RenderingHints( finalMap );
																															}

																															private Map< RenderingHints.Key, List< Object > >
																															getRenderingHintsMap()
																															{
																																ArrayList< RenderingHints.Key > keys
																																= new ArrayList< RenderingHints.Key >();

																																List< Object > values = new LinkedList< Object >();

																																for( Field f : RenderingHints.class.getFields() )
																																{
																																	try
																																	{
																																		if( f.getType() == RenderingHints.Key.class )
																																		{
																																			RenderingHints.Key key
																																			= (RenderingHints.Key) f.get( null );

																																			keys.add( key );
																																		}
																																		else if( f.getName().startsWith( "VALUE_" ) ) //$NON-NLS-1$
																																		{
																																			Object value = f.get( null );

																																			values.add( value );
																																		}
																																	}
																																	catch( IllegalAccessException e )
																																	{
																																		// Should not happen
																																		throw new Error( e );
																																	}
																																}

																																Map< RenderingHints.Key, List< Object > > mapping
																																= new HashMap< RenderingHints.Key, List< Object > >();

																																for( Object value : values )
																																{
																																	for( RenderingHints.Key key : mapping.keySet() )
																																	{
																																		if( key.isCompatibleValue( value ) )
																																		{
																																			List< Object > l = mapping.get( key );

																																			if( l == null )
																																			{
																																				l = new ArrayList< Object >();

																																				mapping.put( key, l );
																																			}

																																			l.add( value );
																																		}
																																	}
																																}

																																return mapping;
																															}
																																};

																																Generator< GlyphVector > GLYPH_VECTOR = new Generator< GlyphVector >()
																																		{
																																	public GlyphVector getValue()
																																	{
																																		Font f = FONT.getValue();
																																		FontRenderContext context = FONT_RENDER_CONTEXT.getValue();

																																		return f.createGlyphVector( context, STRING.getValue() );
																																	}
																																		};

																																		Generator< FontRenderContext > FONT_RENDER_CONTEXT
																																		= new Generator< FontRenderContext >()
																																		{
																																			public FontRenderContext getValue()
																																			{
																																				return new FontRenderContext( Generator.AFFINE_TRANSFORM.getValue(),
																																						RAND.nextBoolean(), RAND.nextBoolean() );
																																			}
																																		};

																																		Generator< Polygon > POLYGON = new Generator< Polygon >()
																																				{
																																			public Polygon getValue()
																																			{
																																				int[] x = ArrayGenerator.INT.getValue( 10 );
																																				int[] y = ArrayGenerator.INT.getValue( 12 );
																																				return new Polygon( x, y, RAND.nextInt( 11 ) );
																																			}
																																				};

																																				Generator< Image > IMAGE = new Generator< Image >()
																																						{
																																					public Image getValue()
																																					{
																																						return new BufferedImage(
																																								RAND.nextInt( 10 ) + 1, RAND.nextInt( 10 ) + 1,
																																								BufferedImage.TYPE_BYTE_BINARY );
																																					}
																																						};

																																						Generator< Graphics > GRAPHICS = new Generator< Graphics >()
																																								{
																																							public Graphics getValue()
																																							{
																																								return IMAGE.getValue().getGraphics();
																																							}
																																								};

																																								Generator< GraphicsConfiguration > GRAPHICS_CONFIGURATION
																																								= new Generator< GraphicsConfiguration >()
																																								{
																																									public GraphicsConfiguration getValue()
																																									{
																																										GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();

																																										GraphicsDevice[] device = env.getScreenDevices();

																																										GraphicsConfiguration[] configs
																																										= device[ RAND.nextInt( device.length ) ].getConfigurations();

																																										return configs[ RAND.nextInt( configs.length ) ];
																																									}
																																								};
	}

	public static class ArrayGenerator< T > implements Generator< T >
	{
		/**
		 * The class for the elements generated.
		 */
		private final Class< ? > elementType;

		public ArrayGenerator( Class< T > arrayType )
		{
			if( arrayType.isArray() )
			{
				elementType = arrayType.getComponentType();
			}
			else
			{
				String message = getString( "NonArray" ); //$NON-NLS-1$
				throw new IllegalArgumentException( message );
			}
		}

		public Class<?> getElementType()
		{
			return elementType;
		}

		public T getValue()
		{
			return getValue( RAND.nextInt( MAXIMUM_ARRAY_SIZE + 1 ) );
		}

		@SuppressWarnings( "unchecked" )
		public T getValue( final int size )
		{
			final Object array = Array.newInstance( elementType, size );

			for( int i = 0; i < size; i++ )
			{
				Array.set( array, i, getRandomValue( elementType ) );
			}

			return (T) array;
		}

		public static final ArrayGenerator< boolean[] > BOOLEAN
		= new ArrayGenerator< boolean[] >( boolean[].class )
		{
			@Override
			public boolean[] getValue( int size )
			{
				boolean[] array = new boolean[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = RAND.nextBoolean();
				}

				return array;
			}
		};

		public static final ArrayGenerator< byte[] > BYTE
		= new ArrayGenerator< byte[] >( byte[].class )
		{
			@Override
			public byte[] getValue( final int size )
			{
				final byte[] array = new byte[ size ];

				RAND.nextBytes( array );

				return array;
			}
		};

		public static final ArrayGenerator< short[] > SHORT
		= new ArrayGenerator< short[] >( short[].class )
		{
			@Override
			public short[] getValue( int size )
			{
				final short[] array = new short[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = (short) RAND.nextInt( 1 << 16 );
				}

				return array;
			}
		};

		public static final ArrayGenerator< char[] > CHAR
		= new ArrayGenerator< char[] >( char[].class )
		{
			@Override
			public char[] getValue( final int size )
			{
				char[] array = new char[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = (char) RAND.nextInt( 1 << 16 );
				}

				return array;
			}
		};


		public static final ArrayGenerator< int[] > INT
		= new ArrayGenerator< int[] >( int[].class )
		{
			@Override
			public int[] getValue( final int size )
			{
				int[] array = new int[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = RAND.nextInt();
				}

				return array;
			}
		};


		public static final ArrayGenerator< long[] > LONG
		= new ArrayGenerator< long[] >( long[].class )
		{
			@Override
			public long[] getValue( final int size )
			{
				long[] array = new long[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = RAND.nextLong();
				}

				return array;
			}
		};

		public static final ArrayGenerator< float[] > FLOAT
		= new ArrayGenerator< float[] >( float[].class )
		{
			@Override
			public float[] getValue( final int size )
			{
				float[] array = new float[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = RAND.nextFloat();
				}

				return array;
			}
		};

		public static final ArrayGenerator< double[] > DOUBLE
		= new ArrayGenerator< double[] >( double[].class )
		{
			@Override
			public double[] getValue( final int size )
			{
				double[] array = new double[ size ];

				for( int i = 0; i < array.length; i++ )
				{
					array[ i ] = RAND.nextDouble();
				}

				return array;
			}
		};
	}

	/**
	 * Creates the map from Class to the random object generator
	 * for that class.
	 * @return Map that maps from a class to and instance of
	 * <code>RandomData.Generator</code> that generates instances of
	 * that class.
	 */
	private static Map< Class< ? >, Generator< ? > > getGeneratorMap()
	{
		final Map< Class<?>, Generator< ? > > m
		= new HashMap< Class< ? >, Generator< ? > >();

		m.put( boolean.class, Generator.BOOLEAN );
		m.put( Boolean.class, Generator.BOOLEAN );
		m.put( boolean[].class, ArrayGenerator.BOOLEAN );
		m.put( byte.class, Generator.BYTE );
		m.put( Byte.class, Generator.BYTE );
		m.put( byte[].class, ArrayGenerator.BYTE );
		m.put( short.class, Generator.SHORT );
		m.put( Short.class, Generator.SHORT );
		m.put( short[].class, ArrayGenerator.SHORT );
		m.put( char.class, Generator.CHARACTER );
		m.put( Character.class, Generator.CHARACTER );
		m.put( char[].class, ArrayGenerator.CHAR );
		m.put( int.class, Generator.INTEGER );
		m.put( Integer.class, Generator.INTEGER );
		m.put( int[].class, Generator.INTEGER );
		m.put( long.class, Generator.LONG );
		m.put( Long.class, Generator.LONG );
		m.put( long[].class, ArrayGenerator.LONG );
		m.put( float.class, Generator.FLOAT );
		m.put( Float.class, Generator.FLOAT );
		m.put( float[].class, ArrayGenerator.FLOAT );
		m.put( double.class, Generator.DOUBLE );
		m.put( Double.class, Generator.DOUBLE );
		m.put( double[].class, ArrayGenerator.DOUBLE );
		m.put( Object.class, Generator.OBJECT );
		m.put( String.class, Generator.STRING );
		m.put( Color.class, Generator.COLOR );
		m.put( AffineTransform.class, Generator.AFFINE_TRANSFORM );
		m.put( GlyphVector.class, Generator.GLYPH_VECTOR );
		m.put( RenderingHints.Key.class, Generator.RENDERING_HINTS_KEY );
		m.put( RenderingHints.class, Generator.RENDERING_HINTS );
		m.put( Rectangle.class, Generator.RECTANGLE );
		m.put( Font.class, Generator.FONT );
		m.put( FontMetrics.class, Generator.FONT_METRICS );
		m.put( FontRenderContext.class, Generator.FONT_RENDER_CONTEXT );
		m.put( Polygon.class, Generator.POLYGON );
		m.put( GraphicsConfiguration.class, Generator.GRAPHICS_CONFIGURATION );
		m.put( Image.class, Generator.IMAGE );
		m.put( BufferedImage.class, Generator.IMAGE );
		m.put( Graphics.class, Generator.GRAPHICS );

		return m;
	}

	/**
	 * Get a generator of random instances of the given class.
	 * @param <T> The type of objects to be generated by the
	 * <code>Generator</code>.
	 * @param c The class instance for the type to be generated.
	 * @return A <code>Generator</code> instance that generates
	 * instances of the given class.
	 * @throws IllegalArgumentException If this classs does not know
	 * how to generate instances of the given class.
	 */
	@SuppressWarnings( "unchecked" )
	public static < T > Generator< T > getRandomGenerator( final Class< T > c )
			throws IllegalArgumentException
			{
		Generator< T > generator = (Generator< T >) generatorMap.get( c );

		if( generator == null )
		{
			if( c.isArray() )
			{
				generator = new ArrayGenerator< T >( c );
			}
			else
			{
				final String message = getString( "NoGenerator", c ); //$NON-NLS-1$
				throw new IllegalArgumentException( message );
			}
		}

		return generator;
			}

	public static < T > T getRandomValue( final Class< T > c )
	{
		return getRandomGenerator( c ).getValue();
	}

	private static final Map< Class< ? >, Generator< ? > >
	generatorMap = getGeneratorMap();
}
