package com.github.nobleworks.util;

/**
 * A timer that acts like a stopwatch to measure elapsed time. It can be stopped
 * and restarted.
 * 
 * @author Dale King, Nobleworks Software
 */
public class ElapsedTimer
{
	/**
	 * The start time for the timer. This value is subtracted from either the
	 * current time or the ending time depending on whether the timer is
	 * running to determine elapsed time.
	 */
	private long startTime;

	/**
	 * The ending time for the timer. This value is only used when the when the
	 * timer is not running.
	 */
	private long endTime;

	/** Flag indicating whether the time is running. */
	private boolean running = false;

	/**
	 * Constructor which merely calls reset.
	 */
	public ElapsedTimer()
	{
		reset();
	}

	/**
	 * Resets the timer to not running with no time elapsed.
	 */
	public synchronized void reset()
	{
		// Indicate that we are not running
		running = false;


		// Set start and end times to the same value to indicate no
		// time has elapsed
		startTime = System.currentTimeMillis();
		endTime = startTime;
	}

	/**
	 * Gets the current amount of elapsed time in milliseconds.
	 *
	 * @return The time that has elapsed since the start.
	 */
	public final synchronized long getElapsedTime()
	{
		long elapsed;

		if( running )
		{
			// If we are running subtract the current time
			// from the start time.
			elapsed = System.currentTimeMillis() - startTime;
		}
		else
		{
			// If we're not running, subtract the time we stopped
			// from the time we started.
			elapsed = endTime - startTime;
		}

		return elapsed;
	}

	/**
	 * Start or restart the timer running. If the timer had previously been
	 * running and was stopped, calling this method will cause additional time
	 * to accumulate. Does nothing if the timer is already running.
	 */
	public synchronized void start()
	{
		// Do nothing if the timer is running
		if( !running )
		{
			// Move the startTime forward by the amount of time
			// the timer has been stopped. This subtracts out the
			// time that the timer was stopped.
			startTime += ( System.currentTimeMillis() - endTime );


			// Switch to the running state
			running = true;
		}
	}

	/**
	 * Stops the timer from running without clearing the elapsed time.
	 */
	public synchronized void stop()
	{
		// Remember the time at which we stopped it
		endTime = System.currentTimeMillis();


		// Stop it from running
		running = false;
	}
}
