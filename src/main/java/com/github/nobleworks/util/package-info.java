/**
 * Some general purpose utility classes.
 * @author DaleKing, Nobleworks Software
 */
package com.github.nobleworks.util;