package com.github.nobleworks.util;

/**
 * This class performs exact string searches using the Boyer-Moore algorithm.
 * The Boyer-Moore algorithm can perform very efficient searches. It has the
 * peculiar characteristic that it is more efficient the longer the search
 * string is. There is overhead associated with it, so it should only be used in
 * cases where the string you are searching for is long, the text you are
 * searching is long, or you repeatedly search for the same string, the extra
 * efficiency can offset the added overhead. From some simple testing against
 * String.indexOf, it appears that Boyer Moore wins when the text being searched
 * for is around 10 characters or more. It loses quite badly for text that is
 * only a few characters. Your mileage may vary.
 * <p>
 * It maintains some state information on the text being searched for. It
 * preprocesses the search text and creates a table with information for
 * skipping through the string based on the characters of the search string.
 * <p>
 * This implementation uses CharSequence for the search string and for the
 * string to be searched. This allows you to use any source of text, not just a
 * java.util.String.
 * <p>
 * For a description of the theory behind the algorithm, simply do a web search
 * on Boyer Moore search.
 * <p>
 * Here is an example of use:
 * 
 * <pre>
 * String pattern = &quot;bed&quot;;
 * BoyerMooreSearch bms = new BoyerMooreSearch(pattern);
 * String target = &quot;bedbeedbedbbeds&quot;;
 * for (int index = 0; (index = bms.search(target, index)) &gt;= 0; index++) {
 * 	System.out.println(index);
 * 	System.out.println(target);
 * 	for (int i = 0; i &lt; index; i++) {
 * 		System.out.print(&quot; &quot;);
 * 	}
 * 	System.out.println(pattern);
 * }
 * </pre>
 * 
 * This code will print:
 * 
 * <pre>
 * 0
 * bedbeedbedbbeds
 * bed
 * 7
 * bedbeedbedbbeds
 *        bed
 * 11
 * bedbeedbedbbeds
 *            bed
 * </pre>
 * 
 * Note that if you modify the CharSequence for the search pattern, you must
 * call setPattern for the changes to take effect. This class will behave
 * unpredictably if this is not done.
 * 
 * @author Dale King
 */
public class BoyerMooreSearch
{
	/**
	 * The text we are searching for.
	 */
	private CharSequence pattern;

	/**
	 * Table that contains the distance of the last occurrence of a character from
	 * the beginning of the search pattern plus one. It contains 0 for characters
	 * that are not in the search pattern.
	 */
	private int[] delta = new int[ Byte.MAX_VALUE - Byte.MIN_VALUE ];

	/**
	 * Create a new BoyerMooreSearch object without setting the search
	 * pattern. The pattern will have to be set using setPattern before
	 * it will be of much use.
	 */
	public BoyerMooreSearch()
	{
	}

	/**
	 * Create a BoyerMooreSearch to search for the given pattern.
	 * @param pattern Text to search for
	 */
	public BoyerMooreSearch( CharSequence pattern )
	{
		setPattern( pattern );
	}
	/**
	 * Gets the text pattern that is to be searched for.
	 * @return The pattern used for searching.
	 */
	public CharSequence getPattern()
	{
		return pattern;
	}

	/**
	 * Search the given target for the first occurrence of the pattern.
	 * 
	 * @param target
	 *            Text to search
	 * @return Index of the first occurrence of the pattern string or -1 if it
	 *         is not found.
	 */
	public int search( CharSequence target )
	{
		return search( target, 0 );
	}

	/**
	 * Search the given target for the pattern, beginning at the given index.
	 *
	 * @param target Text to search.
	 * @param index Index within the text of where to start text. If the index does not
	 * point within the target string, this routine returns -1.
	 * @return Index of the first occurrence of the pattern string or -1 if
	 * it is not found.
	 */
	public int search( CharSequence target, int index )
	{
		int targetLength = target.length();

		// No need to search if index outside of the string, or pattern not set.
		if( 0 <= index  && index < targetLength && pattern != null )
		{
			// Save the length of pattern
			int patternLength = pattern.length();

			// If pattern is empty string, say it was found at the starting index.
			if( patternLength == 0 )
			{
				return index;
			}

			// Save the last character of the pattern into a local variable
			// since we need that several times.
			char lastChar = pattern.charAt( patternLength - 1 );

			// Iterate through the target string, until we go past the end
			for( int targetIndex = index + patternLength - 1; targetIndex < target.length(); )
			{
				char c = target.charAt( targetIndex );

				// If this character matches the last character in pattern, check
				// for match.
				if( c == lastChar )
				{
					int patternIndex = patternLength - 1;

					// Check the rest of the characters
					do
					{
						// If we have gone through the entire pattern we found a match,
						// return its index.
						if( patternIndex <= 0 )
						{
							return targetIndex;
						}

						// Move to the previous character in pattern and target
						patternIndex -= 1;
						targetIndex -= 1;

						c = target.charAt( targetIndex );

					} while( c == pattern.charAt( patternIndex ) );

					// If we get here we didn't match, skip ahead by the length
					// of the pattern
					targetIndex += patternLength;

					// Check the character that didn't match. If it is in the
					// pattern string we need to back up to line up with the last
					// occurrence in the pattern except if we already searched past
					// that point, in which case we only back up by how much more
					// we had to search.
					if( c < delta.length )
					{
						targetIndex -= Math.min( delta[ c ], patternIndex );
					}
				}
				else
				{
					// If we didn't match last character skip ahead the entire length of the
					// pattern.
					targetIndex += patternLength;

					// See if the target character was one of the characters in the
					// pattern. If so we backup to line up the target character with
					// the last occurrence of the character in the pattern.
					if( c < delta.length )
					{
						targetIndex -= delta[ c ];
					}
				}
			}
		}

		// Not found, return -1 to indicate not found.
		return -1;
	}
	/**
	 * Sets the pattern to search for..
	 * @param newPattern Text that is to be searched for.
	 */
	public final void setPattern( final CharSequence newPattern )
	{
		// Clear any previous information out of the delta array.
		java.util.Arrays.fill( delta, 0 );

		// Set the pattern.
		pattern = newPattern;

		int patlen = pattern.length();

		// Initialize the delta array from the pattern.
		for( int j = 0; j < patlen; j++ )
		{
			char c = pattern.charAt( j );

			// Grow the array if necessary
			if( c >= delta.length )
			{
				// Try doubling the array
				int newCapacity = delta.length * 2 + 1;

				// If that is not large enough make it just big enough.
				if( c >= newCapacity )
				{
					newCapacity = c + 1;
				}

				// Don't go over the number of possible characters
				newCapacity = Math.max( newCapacity, Character.MAX_VALUE + 1 );

				delta = Arrays.resize( delta, newCapacity );
			}

			// Set the distance of this character.
			delta[ c ] = j + 1;
		}
	}
}
