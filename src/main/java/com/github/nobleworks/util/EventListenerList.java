package com.github.nobleworks.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;

/**
 * A class that holds a list of event listeners.
 * @param <T> The type for the event listeners in the list.
 *
 * @author Dale King, Nobleworks Software
 */
@SuppressWarnings("serial")
public class EventListenerList< T > implements Serializable, Iterable< T >
{
	/**
	 * A null array to be shared by all empty listener lists.
	 */
	private static final Object[] NULL_ARRAY = new Object[ 0 ];

	/**
	 * The list of listeners in the list.
	 */
	@SuppressWarnings( "unchecked" )
	protected transient T[] listenerList = (T[]) NULL_ARRAY;

	/**
	 * Adds the given listener to the list.
	 * @param l the listener to be added
	 */
	@SuppressWarnings( "unchecked" )
	public synchronized void add( T l )
	{
		if( l != null )
		{
			if( listenerList == NULL_ARRAY )
			{
				// if this is the first listener added,
				// initialize the lists
				listenerList = (T[]) new Object[] { l };
			}
			else
			{
				// Otherwise copy the array and add the new listener
				final int oldLength = listenerList.length;

				T[] tmp = Arrays.resize( listenerList, oldLength + 1 );

				tmp[ oldLength ] = l;

				listenerList = tmp;
			}
		}
	}

	/**
	 * Removes the listener from the list.
	 * @param l the listener to be removed
	 */
	@SuppressWarnings( "unchecked" )
	public synchronized void remove( T l )
	{
		if( l != null )
		{
			// Is l on the list?
			for( int i = 0; i < listenerList.length; i++ )
			{
				if( l.equals( listenerList[ i ] ) )
				{
					if( listenerList.length == 1 )
					{
						listenerList = (T[]) NULL_ARRAY;
					}
					else
					{
						T[] tmp = (T[]) new Object[ listenerList.length - 1 ];

						// Copy the list up to index
						System.arraycopy( listenerList, 0, tmp, 0, i );

						// Copy the list after the index, but not if it was last
						if( i < tmp.length )
						{
							System.arraycopy( listenerList, i + 1, tmp, i,
									tmp.length - i );
						}

						// set the listener array to the new array
						listenerList = tmp;
					}

					break;
				}
			}
		}
	}

	/**
	 * Get an iterator for the listeners in the list. This iterator will
	 * reflect the listeners at the time of the call and will be unaffected
	 * by subsequent changes to the list.
	 * @return An iterator for the listners currently in the list.
	 * @see Iterable.iterator
	 */
	public Iterator< T > iterator()
	{
		return Arrays.iterator( listenerList );
	}

	/**
	 * Method for serializing the list of listeners.
	 * @param s Where to serialize the contents to.
	 * @throws IOException If an error occurs.
	 */
	private void writeObject( ObjectOutputStream s ) throws IOException
	{
		s.defaultWriteObject();

		// Save the non-null event listeners:
		for( T l : listenerList )
		{
			if( l != null && l instanceof Serializable )
			{
				s.writeObject( l );
			}
		}

		s.writeObject( null );
	}

	/**
	 * Deserialize a list of objects from the given
	 * <code>ObjectInputStream</code>.
	 * @param s The <code>ObjectInputStream</code> from which to deseralize
	 * the list.
	 * @throws IOException If there is an error reading.
	 * @throws ClassNotFoundException If the classes read cannot be found.
	 */
	@SuppressWarnings( "unchecked" )
	private void readObject( ObjectInputStream s ) throws IOException, ClassNotFoundException
	{
		listenerList = (T[]) NULL_ARRAY;

		s.defaultReadObject();

		for( Object l = s.readObject(); l != null; l = s.readObject() )
		{
			add( (T) l );
		}
	}

	/**
	 * Returns a string representation of the EventListenerList.
	 * @return String representation of the listeners in the list.
	 */
	@Override
	public String toString()
	{
		T[] list = listenerList;

		StringBuilder s = new StringBuilder( "EventListenerList: " );
		s.append( list.length + " listeners: " );

		for( T listener : list )
		{
			s.append( " listener " + listener );
		}

		return s.toString();
	}
}
