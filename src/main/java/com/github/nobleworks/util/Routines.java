package com.github.nobleworks.util;

import java.lang.reflect.Array;
import java.util.Random;

/**
 * Some general purpose utility routines.
 *
 * @author Dale King, Nobleworks Software
 */
public final class Routines
{
    /**
     * Random number generator for shuffling.
     */
    private static Random random = new Random();

    /**
     * This method counts the number of bits set within the given integer.
     * Given an n-bit value with k of those bits set, the efficiency of this
     * algorithm is O(k) rather than the O(n) of an algorithm that simply
     * looped through all bits counting non zero ones.
     *
     * @param value Value whose bits are to be counted
     *
     * @return Number of non-zero bits in value
     */
    public static int countBitsSet( final int value )
    {
        int count = 0;
        int temp = value;

        while( temp != 0 )
        {
            // The result of this operation is to subtract off
            // the least significant non-zero bit. This can be seen
            // from noting that subtracting 1 from any number causes
            // all bits up to and including the least significant
            // non-zero bit to be complemented.
            //
            // For example:
            //              temp = 10101100
            //          temp - 1 = 10101011
            // (temp - 1) & temp = 10101000
            temp &= ( temp - 1 );

            count++;
        }

        return count;
    }

    /**
     * This method counts the number of bits set within the given long. Given an
     * n-bit value with k of those bits set, the efficiency of this algorithm is
     * O(k) rather than the O(n) of an algorithm that simply looped through all
     * bits counting non zero ones.
     *
     * @param value Value whose bits are to be counted
     *
     * @return Number of non-zero bits in value
     */
    public static int countBitsSet( final long value )
    {
        int count = 0;
        long temp = value;

        while( temp != 0 )
        {
            // The result of this operation is to subtract off
            // the least significant non-zero bit. This can be seen
            // from noting that subtracting 1 from any number causes
            // all bits up to and including the least significant
            // non-zero bit to be complemented.
            //
            // For example:
            //              temp = 10101100
            //          temp - 1 = 10101011
            // (temp - 1) & temp = 10101000
            temp &= ( temp - 1 );

            count++;
        }

        return count;
    }

    /**
     * Perform a random shuffle on the given array. It works with
     * any array type, including arrays of primitives.
     *
     * @param array The array to be shuffled.
     * @throws IllegalArgumentException if the parameter is not an array
     */
    public static void shuffle( final Object array )
    {
        final int length = Array.getLength( array );

        for( int i = length; i > 1; i-- )
        {
            final int location = random.nextInt( i );
            final Object temp1 = Array.get( array, i - 1 );
            final Object temp2 = Array.get( array, location );
            Array.set( array, i - 1, temp2 );
            Array.set( array, location, temp1 );
        }
    }
}
