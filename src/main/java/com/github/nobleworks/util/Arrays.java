package com.github.nobleworks.util;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Utility class for dealing with arrays. It has overloads for all the primitive
 * types and genrics for arrays of objects.
 * 
 * The functions it supports are:
 * <ul>
 * <li>Resizing arrays to a specific length.</li>
 * <li>Extracting part of an array into a new array.</li>
 * <li>Lengthening an array if necessary to a minimum size.</li>
 * <li>Creating an Iterator for an array.</li>
 * <li>Creating an Iterable for an array.</li>
 * 
 * Since it is not actually possible to change the size of an existing array,
 * the resize, extract, and lengthen arrays all return the result of the
 * operation which may be the same as the input array. So they are usually used
 * as in this example:
 * 
 * <pre>
 * array = Arrays.resize(array, newLength);
 * </pre>
 * 
 * </ul>
 * 
 * @author Dale King, Nobleworks Software
 */
public final class Arrays
{
    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param <T> The type of the array elements.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static <T> T[] resize( T[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static boolean[] resize( boolean[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static byte[] resize( byte[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static short[] resize( short[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static char[] resize( char[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static int[] resize( int[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static long[] resize( long[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static float[] resize( float[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Resize the given array to the given length. The resized array is
     * returned. This method will return the array unchanged if it is already
     * the desired length. To resize an array and have it always return a
     * new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param newLength The desired length for the array. Must be
     * non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static double[] resize( double[] array, int newLength )
    {
        if (array.length != newLength)
        {
            array = java.util.Arrays.copyOf(array, newLength);
        }

        return array;
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param <T> The type of the array elements.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static <T> T[] lengthen( T[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static boolean[] lengthen( boolean[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static byte[] lengthen( byte[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static short[] lengthen( short[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static char[] lengthen( char[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static int[] lengthen( int[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static long[] lengthen( long[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static float[] lengthen( float[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Lengthen the given array to the given minimum length. The resized
     * array is returned. This method will return the array unchanged if it
     * is already as long as the desired length. To resize an array and have
     * it always return a new array, use the <code>extract</code> method.
     * @param array The array to be resized. Must be non-null.
     * @param minimumLength The desired minimum length for the array.
     * Must be non-negative.
     * @return An array of the given size. If the array is already the correct
     * size it will be returned unchanged.
     */
    public static double[] lengthen( double[] array, int minimumLength )
    {
        return resize(array, Math.max(minimumLength, array.length));
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param <T>
     *            The type for the array.
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static <T> T[] extract( T[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static boolean[] extract( boolean[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static byte[] extract( byte[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static short[] extract( short[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static char[] extract( char[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static int[] extract( int[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static long[] extract( long[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static float[] extract( float[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Creates a new array of the given length and copies into it elements from
     * the given array beginning at the given offset. Unlike the other resize
     * methods. This method always returns a new array.
     * 
     * @param array
     *            The array from which to copy elements from. Must be non-null.
     * @param offset
     *            The starting offset of data to be preserved. Must be
     *            non-negative.
     * @param newLength
     *            The length for the new array. Must be non-negative.
     * @return An array instance
     * @deprecated The same functionality is provided by
     * @see(java.util.Arrays#copyOfRange) which this is just now a wrapper for.
     */
    @Deprecated
    public static double[] extract( double[] array, int offset, int newLength )
    {
        return java.util.Arrays.copyOfRange(array, offset, offset + newLength);
    }

    /**
     * Superclass for an iterator over an array.
     * @author Dale King
     * @param <T> The type for the elements returned by the iterator.
     */
    private abstract static class AbstractArrayIterator< T >
    implements Iterator< T >
    {
        /** Location counter with the index of the next entry to return. */
        private int count = 0;

        /** The length of the array, cached so we only have to look it up once. */
        private final int length;

        /**
         * Construct an Iterator for the given array.
         *
         * @param length The length of the array over which to iterate.
         */
        private AbstractArrayIterator( int length )
        {
            this.length = length;
        }

        /**
         * Returns <code>true</code> if the iteration has more elements.
         * @see java.util.Iterator#hasNext()
         * @return <code>true</code> if there are more values to iterate over.
         */
        public boolean hasNext()
        {
            return count < length;
        }

        /**
         * Get the element at the given index from the array.
         * @param index The index of the element to get.
         * @return The element at the given index in the array.
         */
        protected abstract T get( int index );

        /**
         * Returns the next element of the array.
         * @see java.util.Iterator#next()
         * @return The next item in the array.
         * @throws NoSuchElementException If there are no more elements.
         */
        public T next()
        {
            if( hasNext() )
            {
                return get( count++ );
            }
            else
            {
                throw new NoSuchElementException();
            }
        }

        /**
         * Throws an <code>UnsupportedOperationException</code>.
         * @throws UnsupportedOperationException since remove is not supported.
         */
        public void remove() throws UnsupportedOperationException
        {
            String message = Messages.getString( "RemoveNotSupported" ); //$NON-NLS-1$
            throw new UnsupportedOperationException( message );
        }
    }

    /**
     * Class for iterating over an array of objects.
     * 
     * @author Dale King, Nobleworks Software.
     * 
     * @param <T>
     *            The element type of the array.
     */
    private static final class ObjectArrayIterator< T >
    extends AbstractArrayIterator< T >
    {
        /** The actual array we are iterating over. */
        private final T[] array;

        /**
         * Returns the number of elements to iterate over in the given array.
         * @param <T> The element type of the array
         * @param array The array to determine the length of.
         * @return The length of the array or zero if the array is null.
         */
        private static < T > int length( T[] array )
        {
            int result;

            if( array == null )
            {
                result = 0;
            }
            else
            {
                result = array.length;
            }

            return result;
        }

        /**
         * Construct an Iterator for the given array.
         *
         * @param array The array over which to iterate.
         */
        public ObjectArrayIterator( T[] array )
        {
            super( length( array ) );

            this.array = array;
        }

        /**
         * Get the object at the given index in the array.
         * @param index The index of the element to return.
         * @return The object at the given index in the array.
         */
        @Override
        protected T get( int index )
        {
            return array[ index ];
        }
    }

    /**
     * Object that returns iterators for an array of objects. Can be used in
     * a for-each statement.
     * @author Dale King
     *
     * @param <T> The type of objects returned by the iterator.
     */
    private static final class ObjectArrayIterable< T > implements Iterable< T >
    {
        /** The actual array we are iterating over. */
        private final T[] array;

        /**
         * Construct an Iterable for the given array.
         *
         * @param array The array over which to iterate.
         */
        private ObjectArrayIterable( T[] array )
        {
            this.array = array;
        }

        /**
         * Return a new iterator for iterating over the contents of the array.
         * @return An iterator for the elements of the array.
         */
        public Iterator< T > iterator()
        {
            return new ObjectArrayIterator<T>( array );
        }
    }

    /**
     * Class for iterating over an array of primitives.
     * 
     * @author Dale King, Nobleworks Software.
     * 
     * @param <T>
     *            The element type of the Iterator. Should be the wrapper type
     *            for the primitive.
     */
    private static class PrimitiveArrayIterator< T >
    extends AbstractArrayIterator< T >
    {
        /** The actual array we are iterating over. */
        private final Object array;

        /**
         * Returns the number of elements to iterate over in the given array.
         * @param array The array to determine the length of.
         * @return The length of the array or zero if the array is null.
         */
        private static int length( Object array )
        {
            int length;

            if( array == null )
            {
                length = 0;
            }
            else
            {
                length = Array.getLength( array );
            }

            return length;
        }

        /**
         * Construct an Iterator for the given array.
         *
         * @param array The array over which to iterate.
         *
         * @throws IllegalArgumentException If the argument is not actually an
         *         array.
         */
        private PrimitiveArrayIterator( Object array )
                throws IllegalArgumentException
                {
            // This will throw an IllegalArgumentException if not an array
            super( length( array ) );

            this.array = array;
                }

        /**
         * Return a new iterator for iterating over the contents of the array.
         * @param index The index of the element to return from the array.
         * @return An iterator for the elements of the array.
         */
        @Override
        @SuppressWarnings( "unchecked" )
        protected T get( int index )
        {
            return (T) Array.get( array, index );
        }
    }

    /**
     * Object that returns iterators for an array of primitives. Can be used in
     * a for-each statement.
     * @author Dale King
     *
     * @param <T> The type of objects returned by the iterator.
     */
    private static class PrimitiveArrayIterable< T > implements Iterable< T >
    {
        /** The actual array we are iterating over. */
        private final Object array;

        /**
         * Construct an Iterable for the given array.
         *
         * @param array The array over which to iterate.
         *
         * @throws IllegalArgumentException If the argument is not actually an
         *         array.
         */
        private PrimitiveArrayIterable( Object array ) throws IllegalArgumentException
        {
            this.array = array;
        }

        /**
         * Return a new iterator for iterating over the contents of the array.
         * @return An iterator for the elements of the array.
         */
        public Iterator< T > iterator()
        {
            return new PrimitiveArrayIterator< T >( array );
        }
    }

    /**
     * Get an <code>Iterator</code> for the given array of objects. The
     * iterator will not support deletion.
     * @param <T> The type for the array
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static <T> Iterator< T > iterator( T[] array )
    {
        return new ObjectArrayIterator<T>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of objects.
     * @param <T> The type for the array
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static <T> Iterable< T >iterable( T[] array )
    {
        return new ObjectArrayIterable<T>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of boolean. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Boolean > iterator( boolean[] array )
    {
        return new PrimitiveArrayIterator<Boolean>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of boolean.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Boolean >iterable( boolean[] array )
    {
        return new PrimitiveArrayIterable<Boolean>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of byte. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Byte > iterator( byte[] array )
    {
        return new PrimitiveArrayIterator<Byte>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of byte.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Byte >iterable( byte[] array )
    {
        return new PrimitiveArrayIterable<Byte>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of short. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Short > iterator( short[] array )
    {
        return new PrimitiveArrayIterator<Short>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of short.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Short >iterable( short[] array )
    {
        return new PrimitiveArrayIterable<Short>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of char. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Character > iterator( char[] array )
    {
        return new PrimitiveArrayIterator<Character>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of char.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Character >iterable( char[] array )
    {
        return new PrimitiveArrayIterable<Character>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of int. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Integer > iterator( int[] array )
    {
        return new PrimitiveArrayIterator<Integer>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of int.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Integer > iterable( int[] array )
    {
        return new PrimitiveArrayIterable<Integer>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of long. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Long > iterator( long[] array )
    {
        return new PrimitiveArrayIterator<Long>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of long.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Long > iterable( long[] array )
    {
        return new PrimitiveArrayIterable<Long>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of float. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Float > iterator( float[] array )
    {
        return new PrimitiveArrayIterator<Float>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of float.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Float > iterable( float[] array )
    {
        return new PrimitiveArrayIterable<Float>( array );
    }

    /**
     * Get an <code>Iterator</code> for the given array of double. The
     * iterator will not support deletion.
     * @param array The array to iterate over.
     * @return An <code>Iterator</code> over the given array.
     */
    public static Iterator< Double > iterator( double[] array )
    {
        return new PrimitiveArrayIterator<Double>( array );
    }

    /**
     * Get an <code>Iterable</code> for the given array of double.
     * @param array The array to iterate over.
     * @return An <code>Iterable</code> that will create <code>Iterator</code> instances
     * over the given array.
     */
    public static Iterable< Double > iterable( double[] array )
    {
        return new PrimitiveArrayIterable<Double>( array );
    }
}
