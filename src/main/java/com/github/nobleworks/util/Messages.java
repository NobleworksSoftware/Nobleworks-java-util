/*
 * Created on Feb 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.github.nobleworks.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Accessor class for localized messages in this package.
 * @author Dale King, Nobleworks Software
 */
public final class Messages
{
	/**
	 * The name of the resource bundle for this package.
	 */
	private static final String BUNDLE_NAME
 = "com.github.nobleworks.util.messages"; //$NON-NLS-1$

	/**
	 * The resource bundle for this package.
	 */
	private static final ResourceBundle RESOURCE_BUNDLE
	= ResourceBundle.getBundle( BUNDLE_NAME );

	/**
	 * Get the localized string for the given key.
	 * @param key The key to look up in the resource bundle.
	 * @return The string for the given key. If the key is not in the
	 * resource bundle returns the key surrounded by exclamation points.
	 */
	public static String getString( final String key )
	{
		String result;

		try
		{
			result = RESOURCE_BUNDLE.getString( key );
		}
		catch( MissingResourceException e )
		{
			result = '!' + key + '!';
		}

		return result;
	}

	/**
	 * Get the localized format string for the given key and formats it
	 * with the given arguments.
	 * @param key The key to look up in the resource bundle.
	 * @param args The arguments to apply to the format string.
	 * @return The formatted output of applying the given arguments against
	 * the format string for the given key. If the key is not in the
	 * resource bundle returns the key surrounded by exclamation points.
	 */
	public static String getString( final String key, final Object... args )
	{
		String result;

		try
		{
			result = String.format( RESOURCE_BUNDLE.getString( key ), args );
		}
		catch( MissingResourceException e )
		{
			result = '!' + key + '!';
		}

		return result;
	}
}
