/* Sapphire.java -- Interface for the Saphire II stream cipher.
 *
 * Dedicated to the Public Domain by the author and inventor
 * (Michael Paul Johnson).  This code comes with no warranty.
 * Use it at your own risk.
 * Ported from the C++ implementation of the Sapphire Stream
 * Cipher 6 August 1995.
 */
package com.github.nobleworks.utility.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * An implementation of the Sapphire II stream cipher, created by
 * Michael Paul Johnson.
 * @author Dale King (ported from C++ code by Michael Paul Johnson).
 */
public final class Sapphire
{
    private static final int HASH_LENGTH = 20;

    private static final int INITIAL_ROTOR_INDEX = 1;

    private static final int INITIAL_LAST_CIPHER_CHAR = 11;

    private static final int INITIAL_LAST_PLAIN_CHAR = 7;

    private static final int INITIAL_AVALANCHE_INDEX = 5;

    private static final int INITIAL_RATCHET_INDEX = 3;

    /**
     * A mask for the significant bits in a byte.
     */
    private static final int BYTE_MASK = ( 2 << Byte.SIZE ) - 1;

    /**
     * The number of different byte values.
     */
    private static final int NUMBER_OF_BYTE_VALUES = 2 << Byte.SIZE;

    private static final int MAX_UNSIGNED_BYTE_VALUE = NUMBER_OF_BYTE_VALUES - 1;

    /**
     * Converts the given byte value to its unsigned value.
     * @param b A byte value from -128 to 127
     * @return An int containing its representation as an
     *         unsigned value 0-255.
     */
    private static int toUnsignedByte( final int b )
    {
        return b & BYTE_MASK;
    }

    /**
     * This contains a permutation of the bytes 0 - 0xFF. Each character
     * that is encrypted or decrypted permutes it a bit more.
     */
    private final byte[] cards = new byte[ NUMBER_OF_BYTE_VALUES ];

    /**
     * This is an index into the cards for permutation that is incremented
     * for every character encrypted or decrypted.
     */
    private int rotor;

    /**
     * This is an index into the cards for permutation that moves erratically
     * for every character encrypted or decrypted.
     */
    private int ratchet;

    /**
     * This is an index into the cards for permutation that is very dependent
     * on the data encrypted or decrypted.
     */
    private int avalanche;

    /**
     * This is the last plain-text byte.
     */
    private int lastPlainChar;

    /**
     * This is the last cipher-text byte.
     */
    private int lastCipherChar;

    /**
     * Creates a new Sapphire cipher object, initializing it to the given key.
     * @param key The key value to set the cipher to.
     */
    public Sapphire( final byte[] key )
    {
        setKey( key );
    }

    /**
     * Clean-up before garbage collection by calling setDefaultKey to erase any
     * key information from RAM.
     */
    @Override
    protected void finalize()
    {
        setDefaultKey();
    }

    /**
     * Decrypt the given byte of data.
     * @param b The cipher-text byte to be decrypted.
     * @return The plain-text representation of the given cipher-text byte.
     */
    public int decrypt( final int b )
    {
        lastPlainChar = translate( b );
        lastCipherChar = b;
        return lastPlainChar;
    }

    /**
     * Encrypt the given byte of data.
     * @param b The plain-text byte to be decrypted.
     * @return The cipher-text representation of the given plain-text byte.
     */
    public int encrypt( final int b )
    {
        lastCipherChar = translate( b );
        lastPlainChar = b;
        return lastCipherChar;
    }

    /**
     * Get the value of the card at the given index. This method is used to
     * compensate for the fact that bytes are signed in Java.
     * @return The card value at the given index.
     * @param index The index for the card to retrieve.
     */
    private int getCard( final int index )
    {
        return toUnsignedByte( cards[ toUnsignedByte( index ) ] );
    }

    /**
     * Set the value of the card at the given index. This method is used to
     * compensate for the fact that bytes are signed in Java.
     * @param index The index for the card to set.
     * @param value The value to store at the given index.
     */
    private void setCard( final int index, final int value )
    {
        cards[ toUnsignedByte( index ) ] = (byte) value;
    }

    /**
     * Write a hash value into the given byte array.
     * @param hash The hash value to add to the output.
     */
    public void hashFinal( byte[] hash )
    {
        for( int i = MAX_UNSIGNED_BYTE_VALUE; i >= 0; i-- )
        {
            encrypt( i );
        }
        for( int i = 0; i < hash.length; i++ )
        {
            hash[ i ] = (byte) encrypt( 0 );
        }
    }

    /**
     * Set up a default key, primarily for hash computation.
     */
    public void setDefaultKey()
    {
        // Initialize the indices and data dependencies.
        rotor = INITIAL_ROTOR_INDEX;
        ratchet = INITIAL_RATCHET_INDEX;
        avalanche = INITIAL_AVALANCHE_INDEX;
        lastPlainChar = INITIAL_LAST_PLAIN_CHAR;
        lastCipherChar = INITIAL_LAST_CIPHER_CHAR;

        // Start with cards all in inverse order.
        for( int i = 0; i < NUMBER_OF_BYTE_VALUES; i++ )
        {
            setCard( i, MAX_UNSIGNED_BYTE_VALUE - i );
        }
    }

    /**
     * Set the key for encryption, decryption, or hashing.
     * @param key The key to use. If null or zero length a default key is set.
     */
    public void setKey( final byte[] key )
    {
        // Key size may be up to 256 bytes.
        // Pass phrases may be used directly, with longer length
        // compensating for the low entropy expected in such keys.
        // Alternatively, shorter keys hashed from a pass phrase or
        // generated randomly may be used. For random keys, lengths
        // of from 4 to 16 bytes are recommended, depending on how
        // secure you want this to be.

        // If we have been given no key, assume the default hash setup.
        if( key == null || key.length == 0 )
        {
            setDefaultKey();
        }
        else
        {
            // Start with cards all in order, one of each.
            for( int i = 0; i < NUMBER_OF_BYTE_VALUES; i++ )
            {
                setCard( i, i );
            }

            // Swap the card at each position with some other card.
            int keypos = 0;         // Start with first byte of user key.
            int rsum = 0;
            for( int i = MAX_UNSIGNED_BYTE_VALUE; i >= 0; i-- )
            {
                int toswap = 0;
                if( i != 0 )
                {
                    int retryLimiter = 0;
                    int mask = 1;               // Select just enough bits.

                    while( mask < i )    // the desired range.
                    {
                        mask = ( mask << 1 ) + 1;
                    }

                    final int maxRetries = 11;
                    do
                    {
                        rsum = getCard( rsum ) + key[ keypos++ ];
                        if( keypos >= key.length )
                        {
                            keypos = 0;         // Recycle the user key.
                            rsum += key.length; // key "aaaa" != key "aaaaaaaa"
                        }
                        toswap = toUnsignedByte( mask & rsum );
                        if( ++retryLimiter > maxRetries )
                        {
                            toswap %= i;     // Prevent very rare long loops.
                        }
                    } while( toswap > i );
                }
                int swaptemp = getCard( i );
                setCard( i, getCard( toswap ) );
                setCard( toswap, swaptemp );
                swaptemp = 0;
                toswap = 0;
            }

            // Initialize the indices and data dependencies.
            // Indices are set to different values instead of all 0
            // to reduce what is known about the state of the cards
            // when the first byte is emitted.

            rotor = getCard( INITIAL_ROTOR_INDEX );
            ratchet = getCard( INITIAL_RATCHET_INDEX );
            avalanche = getCard( INITIAL_AVALANCHE_INDEX );
            lastPlainChar = getCard( INITIAL_LAST_PLAIN_CHAR );
            lastCipherChar = getCard( rsum );
            rsum = 0;
            keypos = 0;
        }
    }

    /**
     * Encrypt or decrypt the given byte. The same routine is used for either.
     * The only difference between encrypting and decrypting is how the
     * lastPlainChar and lastCipherChar variables are updated, which is not
     * done in this routine.
     * @param b The byte to be encrypted or decrypted.
     * @return The encrypted or decrypted byte.
     */
    private int translate( final int b )
    {
        // Picture a single enigma rotor with 256 positions, rewired
        // on the fly by card-shuffling.

        // This cipher is a variant of one invented and written
        // by Michael Paul Johnson in November, 1993.

        // Shuffle the deck a little more.
        ratchet += getCard( rotor++ );
        final int swaptemp = getCard( lastCipherChar );
        setCard( lastCipherChar, getCard( ratchet ) );
        setCard( ratchet, getCard( lastPlainChar ) );
        setCard( lastPlainChar, getCard( rotor ) );
        setCard( rotor, swaptemp );
        avalanche += getCard( swaptemp );

        // Output one byte from the state in such a way as to make it
        // very hard to figure out which one you are looking at.
        return toUnsignedByte( b ^ getCard( getCard( ratchet ) + getCard( rotor ) )
                                ^ getCard( getCard( getCard( lastPlainChar )
                                         + getCard( lastCipherChar )
                                         + getCard( avalanche ) ) ) );
    }

    /**
     * This program is a test driver for the Sapphire Stream Cipher,
     * which is represented in the sapphire object.  This program
     * will encrypt or decrypt a file based on pass phrase key on the
     * command line.  The user interface is a simple positional command
     * line interface:
     * <pre>
     * First parameter:   E to Encrypt, D to Decrypt, H to compute
     *                    Hash or cryptographic check value.
     * Second parameter:  Input file name
     * Third parameter:   Output file name
     * Fourth parameter:  Pass phrase
     * </pre>
     * There is minimal error checking.  A serious application should
     * do more error checking and provide for such things as bad pass
     * phrase detection.
     * @param args Command line arguments
     * @throws IOException If unable to read or write
     */
    public static void main( final String[] args ) throws IOException
    {
        int argIndex = 0;

        if( args.length > 2 )
        {
            char command = Character.toUpperCase( args[ argIndex++ ].charAt( 0 ) );

            if( "DEH".indexOf( command ) >= 0 )
            {
                InputStream infile
                    = new BufferedInputStream( new FileInputStream( args[ argIndex++ ] ) );
                OutputStream outfile
                    = new BufferedOutputStream( new FileOutputStream( args[ argIndex++ ] ) );

                final byte[] userKey;

                if( argIndex < args.length )
                {
                    final StringBuilder keyBuffer = new StringBuilder( args[ argIndex++ ] );

                    while( argIndex < args.length )
                    {
                        keyBuffer.append( ' ' );
                        keyBuffer.append( args[ argIndex++ ] );
                    }

                    userKey = keyBuffer.toString().getBytes();
                }
                else
                {
                    userKey = null;
                }

                final Sapphire streamCipher = new Sapphire( userKey );

                switch( command )
                {
                    case 'E':
                    {
                        encrypt( infile, outfile, streamCipher );
                        break;
                    }
                    case 'D':
                    {
                        decrypt( infile, outfile, streamCipher );
                        break;
                    }
                    case 'H':
                    {
                        final byte[] hash = generateHash( infile, streamCipher );

                        final PrintStream ps = new PrintStream( outfile );

                        ps.println( "Check value for " + args[ 1 ] + " is:" );
                        for( int i = 0; i < hash.length; i++ )
                        {
                            ps.format( "%02X", toUnsignedByte( hash[ i ] ) );
                        }
                        ps.println();
                        break;
                    }
                    default:
                        break;
                }
                streamCipher.setDefaultKey();
                infile.close();
                outfile.close();
                return;
            }
        }
        printUsage( System.err );
    }

    private static void encrypt( InputStream input, OutputStream output, Sapphire cipher )
        throws IOException
    {
        int c = input.read();

        while( c != -1 )
        {
            output.write( cipher.encrypt( c ) );

            c = input.read();
        }
    }

    private static void decrypt( InputStream input, OutputStream output, Sapphire cipher )
        throws IOException
    {
        int c = input.read();

        while( c != -1 )
        {
            output.write( cipher.decrypt( c ) );

            c = input.read();
        }
    }

    private static byte[] generateHash( InputStream input, Sapphire cipher ) throws IOException
    {
        int c = input.read();

        while( c != -1 )
        {
            cipher.encrypt( c );

            c = input.read();
        }

        final byte[] hash = new byte[ HASH_LENGTH ];

        cipher.hashFinal( hash );

        return hash;
    }

    private static void printUsage( final PrintStream output )
    {
        output.println( "Sapphire test program" );
        output.println( "Don't forget your passphrase.  No passphrase, no data." );
        output.println();
        output.println( "Syntax:" );
        output.println( "java sapphire E|D|H inname outname passphrase" );
        output.println( "  E = Encrypt" );
        output.println( "  D = Decrypt" );
        output.println( "  H = compute keyed cryptographic Hash" );
        output.println(  );
        output.println( "  passphrase is optional." );
    }
}
