package com.github.nobleworks.utility.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.Icon;

/**
 * This class is an Icon implementation which is created from a rectangular
 * clipping from another icon.
 * 
 * @author Dale King, Nobleworks Software
 */
public class SubIcon implements Icon
{
	private final Rectangle r;
	private Icon sourceIcon;

	public SubIcon( Icon source, int x, int y, int width, int height )
	{
		r = new Rectangle( x, y, width, height );
		sourceIcon = source;
	}

	public SubIcon( Icon source, Rectangle r )
	{
		this.r = new Rectangle( r );
		sourceIcon = source;
	}

	public int getIconHeight()
	{
		return r.height;
	}

	public int getIconWidth()
	{
		return r.width;
	}

	public void paintIcon( Component c, Graphics g, int x, int y )
	{
		Graphics clippedGraphics = g.create( x, y, r.width, r.height );
		sourceIcon.paintIcon( c, clippedGraphics, -r.x, -r.y );
		clippedGraphics.dispose();
	}

	public void setRectangle( Rectangle rectangle )
	{
		r.setBounds( rectangle );
	}

	public void setBounds( int x, int y, int width, int height )
	{
		r.setBounds( x, y, width, height );
	}

	public Rectangle getRectangle()
	{
		return r.getBounds();
	}

	public void setSourceIcon( Icon icon )
	{
		sourceIcon = icon;
	}

	public Icon getSourceIcon()
	{
		return sourceIcon;
	}
}
