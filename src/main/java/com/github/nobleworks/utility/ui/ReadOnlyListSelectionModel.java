package com.github.nobleworks.utility.ui;

import static com.github.nobleworks.util.Messages.getString;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

/**
 * <p>
 * A Read-only selection model wrapper for a list. This can be used to prevent
 * the user from being able to directly change or set the selection for a list.
 * It uses the decorator pattern to wrap the actual list. All methods that would
 * modify the selection state of the model do nothing. All get methods along
 * with methods to add and remove listeners are forwarded on to the wrapped
 * model without modification.
 * </p>
 * <p>
 * This would be used by first creating the actual selection model for the list,
 * creating an instance of this class to wrap the model, the instance of this
 * class is then set as the JList's selection model. The program can manipulate
 * the selection by using the underlying wrapped model.
 * </p>
 * 
 * @author Dale King, Nobleworks Software
 * @see javax.swing.ListSelectionModel
 */
public class ReadOnlyListSelectionModel implements ListSelectionModel
{
	/** The list selection model that this wrapper is wrapping. */
	private final ListSelectionModel wrappedModel;

	/**
	 * Create a read-only selection wrapper for the given selection model.
	 *
	 * @param model The model to wrap.
	 *
	 * @throws NullPointerException If model is null.
	 */
	public ReadOnlyListSelectionModel( ListSelectionModel model )
			throws NullPointerException
			{
		if( model == null )
		{
			String message = getString( "ModelNull" ); //$NON-NLS-1$
			throw new NullPointerException( message );
		}
		else
		{
			wrappedModel = model;
		}
			}

	/**
	 * Returns the minimum selection index of the underlying wrapped model.
	 */
	public int getMinSelectionIndex()
	{
		return wrappedModel.getMinSelectionIndex();
	}

	/**
	 * Returns the maximum selection index of the underlying wrapped model.
	 */
	public int getMaxSelectionIndex()
	{
		return wrappedModel.getMaxSelectionIndex();
	}

	/**
	 * Returns whether the underlying wrapped model value is adjusting.
	 */
	public boolean getValueIsAdjusting()
	{
		return wrappedModel.getValueIsAdjusting();
	}

	/**
	 * Returns the selection mode of the underlying wrapped model.
	 *
	 * @return one of the these values: <ul><li>SINGLE_SELECTION
	 *         <li>SINGLE_INTERVAL_SELECTION  <li>MULTIPLE_INTERVAL_SELECTION
	 *         </ul>
	 */
	public int getSelectionMode()
	{
		return wrappedModel.getSelectionMode();
	}

	/**
	 * Returns whether the given index is selected in the underlying wrapped
	 * model.
	 */
	public boolean isSelectedIndex( int index )
	{
		return wrappedModel.isSelectedIndex( index );
	}

	/**
	 * Returns whether the selection is empty for the underlying wrapped model.
	 */
	public boolean isSelectionEmpty()
	{
		return wrappedModel.isSelectionEmpty();
	}

	/**
	 * Adds the given selection listener to the underlying wrapped model.
	 */
	public void addListSelectionListener( ListSelectionListener l )
	{
		wrappedModel.addListSelectionListener( l );
	}

	/**
	 * Removes the given selection listener from the underlying wrapped model.
	 */
	public void removeListSelectionListener( ListSelectionListener l )
	{
		wrappedModel.removeListSelectionListener( l );
	}

	/**
	 * Does nothing.
	 */
	public void setSelectionMode( int selectionMode )
	{
	}

	/**
	 * Does nothing.
	 */
	public void clearSelection()
	{
	}

	/**
	 * Does nothing.
	 */
	public void setSelectionInterval( int index0, int index1 )
	{
	}

	/**
	 * Does nothing.
	 */
	public void addSelectionInterval( int index0, int index1 )
	{
	}

	/**
	 * Returns the string representation of the underlying wrapped model.
	 */
	@Override
	public String toString()
	{
		return wrappedModel.toString();
	}

	/**
	 * Returns the anchor selection index of the underlying wrapped model.
	 */
	public int getAnchorSelectionIndex()
	{
		return wrappedModel.getAnchorSelectionIndex();
	}

	/**
	 * Returns the lead selection index of the underlying wrapped model.
	 */
	public int getLeadSelectionIndex()
	{
		return wrappedModel.getLeadSelectionIndex();
	}

	/**
	 * Does nothing.
	 */
	public void removeSelectionInterval( int index0, int index1 )
	{
	}

	/**
	 * Does nothing.
	 */
	public void insertIndexInterval( int index, int length, boolean before )
	{
	}

	/**
	 * Does nothing.
	 */
	public void removeIndexInterval( int index0, int index1 )
	{
	}

	/**
	 * Does nothing.
	 */
	public void setValueIsAdjusting( boolean isAdjusting )
	{
	}

	/**
	 * Does nothing.
	 */
	public void setAnchorSelectionIndex( int anchorIndex )
	{
	}

	/**
	 * Does nothing.
	 */
	public void setLeadSelectionIndex( int leadIndex )
	{
	}
}
