package com.github.nobleworks.utility.ui;

import javax.swing.border.TitledBorder;


/**
 * This type exists simply to provide a way to specify a titled border in
 * certain GUI builders. Certain GUI builders (e.g. Visual Age) when allowing
 * you to choose a border only shows those borders that are proper javabeans. To
 * be a proper javabean and be usable in those GUI builders it must have a
 * no-arg constructor. Therefore all this class does is extend TitledBorder to
 * provide a no-arg constructor.
 * 
 * @author Dale King, Nobleworks Software
 */
public class TitledBorderBean extends TitledBorder
{
	/**
	 * The ID for serialization purposes.
	 */
	private static final long serialVersionUID = 3258411750779794739L;

	/**
	 * The no-arg constructor to allow this to be considered a proper java
	 * bean. Calls the TitledBorder( String ) constructor, passing a null.
	 */
	public TitledBorderBean()
	{
		super( (String) null );
	}
}
