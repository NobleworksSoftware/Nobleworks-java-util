package com.github.nobleworks.utility.ui;

import java.awt.Graphics;
import java.awt.Point;

import javax.swing.event.ChangeListener;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;

/**
 * This is a Caret implementation to be used with a text component that you do
 * not wish the user to be able to select or highlight text within. It provides
 * do nothing implementations of all of the Caret methods.
 * 
 * @author Dale King, Nobleworks Software
 */
public class NonSelectableCaret implements Caret
{
	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void addChangeListener( ChangeListener l )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void deinstall( JTextComponent c )
	{
	}

	/**
	 * Returns 0.
	 *
	 * @see javax.swing.text.Caret
	 */
	public int getBlinkRate()
	{
		return 0;
	}

	/**
	 * Returns 0.
	 *
	 * @see javax.swing.text.Caret
	 */
	public int getDot()
	{
		return 0;
	}

	/**
	 * Returns null.
	 *
	 * @see javax.swing.text.Caret
	 */
	public Point getMagicCaretPosition()
	{
		return null;
	}

	/**
	 * Returns 0.
	 *
	 * @see javax.swing.text.Caret
	 */
	public int getMark()
	{
		return 0;
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void install( JTextComponent c )
	{
	}

	/**
	 * Returns false.
	 *
	 * @see javax.swing.text.Caret
	 */
	public boolean isSelectionVisible()
	{
		return false;
	}

	/**
	 * Returns false.
	 *
	 * @see javax.swing.text.Caret
	 */
	public boolean isVisible()
	{
		return false;
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void moveDot( int dot )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void paint( Graphics g )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void removeChangeListener( ChangeListener l )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void setBlinkRate( int rate )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void setDot( int dot )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void setMagicCaretPosition( Point p )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void setSelectionVisible( boolean v )
	{
	}

	/**
	 * Does nothing.
	 *
	 * @see javax.swing.text.Caret
	 */
	public void setVisible( boolean v )
	{
	}
}
