package com.github.nobleworks.utility.ui.i18n;

import static com.github.nobleworks.util.Messages.getString;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;


/**
 * Class for creating a configurable, localized user interface.
 * 
 * @author Dale King, Nobleworks Software
 */
public class UiLocalization
{
	/** Key for look up of mnemonic for action. */
	private static final String MNEMONIC_KEY = "mnemonic"; //$NON-NLS-1$

	/** Key for look up of accelerator for action. */
	private static final String ACCELERATOR_KEY = "accelerator"; //$NON-NLS-1$

	/** Key for look up of text of button for action. */
	private static final String BUTTON_TEXT_KEY = "buttonText"; //$NON-NLS-1$

	/** Key for look up of text of menu item for action. */
	private static final String MENU_TEXT_KEY = "menuText"; //$NON-NLS-1$

	/** Key for look up of tool-tip text for action. */
	private static final String TOOLTIP_TEXT_KEY = "toolTipText"; //$NON-NLS-1$

	/** Key for look up of icon of button for action. */
	private static final String BUTTON_ICON_KEY = "buttonIcon"; //$NON-NLS-1$

	/** Key for look up of icon of menu item for action. */
	private static final String MENU_ICON_KEY = "menuIcon"; //$NON-NLS-1$

	/** Map that maps names to actions. */
	private final Map< String, Action > actionMap = new HashMap< String, Action >();

	/** ResourceBundle for reading configuration. */
	private final ResourceBundle resources;

	/**
	 * Creates a new UiLocalization object.
	 *
	 * @param bundle DOCUMENT ME!
	 */
	public UiLocalization( ResourceBundle bundle )
	{
		resources = bundle;
	}

	/**
	 * @param name
	 * @param key
	 * @return
	 */
	private String getProperty( String name, String key )
	{
		String separator = getString( "NameKeySeparator" ); //$NON-NLS-1$
		return resources.getString( name + separator + key );
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param name DOCUMENT ME!
	 * @param key DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public Icon getIcon( String name, String key )
	{
		Icon icon = null;

		try
		{
			String file = getProperty( name, key );
			ClassLoader loader = getClass().getClassLoader();
			URL url = loader.getResource( file );

			if( url != null )
			{
				icon = new ImageIcon( url );
			}
		}
		catch( MissingResourceException e )
		{
			// Do nothing
		}

		return icon;
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param name DOCUMENT ME!
	 * @param key DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	private KeyStroke getKeyStroke( String name, String key )
	{
		KeyStroke keyStroke;

		try
		{
			final String description = getProperty( name, key );
			keyStroke = KeyStroke.getKeyStroke( description );
		}
		catch( MissingResourceException e )
		{
			keyStroke = null;
		}

		return keyStroke;
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param name DOCUMENT ME!
	 * @param key DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	private String getText( String name, String key )
	{
		String result;

		try
		{
			result = getProperty( name, key );
		}
		catch( MissingResourceException e )
		{
			result = '?' + name + '.' + key + '?';
		}

		return result;
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param a DOCUMENT ME!
	 * @param key DOCUMENT ME!
	 */
	public void configureAction( final Action a, final String key )
	{
		a.putValue( MNEMONIC_KEY, getKeyStroke( key, MNEMONIC_KEY ) );
		a.putValue( ACCELERATOR_KEY, getKeyStroke( key, ACCELERATOR_KEY ) );
		a.putValue( BUTTON_TEXT_KEY, getText( key, BUTTON_TEXT_KEY ) );
		a.putValue( MENU_TEXT_KEY, getText( key, MENU_TEXT_KEY ) );
		a.putValue( TOOLTIP_TEXT_KEY, getText( key, TOOLTIP_TEXT_KEY ) );
		a.putValue( BUTTON_ICON_KEY, getIcon( key, BUTTON_ICON_KEY ) );
		a.putValue( MENU_ICON_KEY, getIcon( key, MENU_ICON_KEY ) );
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param a DOCUMENT ME!
	 */
	public void addAction( final Action a )
	{
		String name = (String) a.getValue( Action.NAME );

		actionMap.put( name, a );
		configureAction( a, name );
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param actions The actions to be added.
	 */
	public void addActions( Action[] actions )
	{
		for( Action a : actions )
		{
			addAction( a );
		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param actions The actions to be added.
	 */
	public void addActions( Iterable< Action > actions )
	{
		for( Action a : actions )
		{
			addAction( a );
		}
	}

	private final class LocalizedMenuItem extends JMenuItem
	{
		private class ChangeListener implements PropertyChangeListener
		{
			public void propertyChange( PropertyChangeEvent e )
			{
				String propertyName = e.getPropertyName();

				if( propertyName.equals( MENU_TEXT_KEY ) )
				{
					setText( (String) e.getNewValue() );
					repaint();
				}
				else if( propertyName.equals( "enabled" ) ) //$NON-NLS-1$
				{
					setEnabled( ( (Boolean) e.getNewValue() ).booleanValue() );
					repaint();
				}
				else if( propertyName.equals( MENU_ICON_KEY ) )
				{
					setIcon( (Icon) e.getNewValue() );
					invalidate();
					repaint();
				}
				else if( propertyName.equals( MNEMONIC_KEY ) )
				{
					setMnemonic( ( (KeyStroke) e.getNewValue() ).getKeyCode() );
					invalidate();
					repaint();
				}
			}
		}

		private static final long serialVersionUID = 3690762799546642486L;

		@Override
		protected void configurePropertiesFromAction( Action a )
		{
			if( a != null )
			{
				setText( (String) a.getValue( MENU_TEXT_KEY ) );
				setIcon( (Icon) a.getValue( MENU_ICON_KEY ) );
				setEnabled( a.isEnabled() );

				KeyStroke key = (KeyStroke) a.getValue( MNEMONIC_KEY );

				if( key != null )
				{
					setMnemonic( key.getKeyCode() );
				}

				setAccelerator(
						(KeyStroke) a.getValue( ACCELERATOR_KEY ) );
			}
		}

		/**
		 * @see AbstractButton#createActionPropertyChangeListener
		 */
		 @Override
		 protected PropertyChangeListener createActionPropertyChangeListener( Action a )
		{
			 return new ChangeListener();
		}
	}

	private class LocalizedButton extends JButton
	{
		private static final long serialVersionUID = 3616729391804331825L;

		@Override
		protected void configurePropertiesFromAction( Action a )
		{
			if( a != null )
			{
				setText( (String) a.getValue( BUTTON_TEXT_KEY ) );

				String tip = (String) a.getValue( TOOLTIP_TEXT_KEY );

				if( ( tip != null ) && ( tip.length() > 0 ) )
				{
					setToolTipText( tip );
				}

				setIcon( (Icon) a.getValue( BUTTON_ICON_KEY ) );
				setEnabled( a.isEnabled() );

				KeyStroke key = (KeyStroke) a.getValue( MNEMONIC_KEY );

				if( key != null )
				{
					setMnemonic( key.getKeyCode() );
				}
			}
		}

		private class ChangeListener implements PropertyChangeListener
		{
			/**
			 * @see java.beans.PropertyChangeListener#propertyChange
			 */
			 public void propertyChange( PropertyChangeEvent e )
			{
				String propertyName = e.getPropertyName();

				if( propertyName.equals( BUTTON_TEXT_KEY ) )
				{
					setText( (String) e.getNewValue() );
					repaint();
				}
				else if( propertyName.equals( TOOLTIP_TEXT_KEY ) )
				{
					String text = (String) e.getNewValue();

					setToolTipText( text );
				}
				else if( propertyName.equals( "enabled" ) ) //$NON-NLS-1$
				{
					Boolean enabledState =
							(Boolean) e.getNewValue();

					setEnabled( enabledState.booleanValue() );
					repaint();
				}
				else if( propertyName.equals( BUTTON_ICON_KEY ) )
				{
					Icon icon = (Icon) e.getNewValue();

					setIcon( icon );
					invalidate();
					repaint();
				}
				else if( propertyName.equals( MNEMONIC_KEY ) )
				{
					KeyStroke key = (KeyStroke) e.getNewValue();

					setMnemonic( key.getKeyCode() );
					invalidate();
					repaint();
				}
			}
		}

		/**
		 * @see javax.swing.AbstractButton#createActionPropertyChangeListener
		 */
		@Override
		protected PropertyChangeListener createActionPropertyChangeListener( Action a )
		{
			return new ChangeListener();
		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param name DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 *
	 * @throws IllegalArgumentException DOCUMENT ME!
	 */
	public AbstractButton getButton( String name ) throws IllegalArgumentException
	{
		Action a = actionMap.get( name );

		if( a == null )
		{
			String message = getString( "ActionNotFound", name ); //$NON-NLS-1$
			throw new IllegalArgumentException( message );
		}
		else
		{
			AbstractButton button =
					new LocalizedButton();

			button.setAction( a );

			return button;
		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param name DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 *
	 * @throws IllegalArgumentException DOCUMENT ME!
	 */
	public JMenuItem getMenuItem( final String name ) throws IllegalArgumentException
	{
		Action menuAction = actionMap.get( name );

		if( menuAction == null )
		{
			String message = getString( "ActionNotFound", name ); //$NON-NLS-1$
			throw new IllegalArgumentException( message );
		}
		else
		{
			JMenuItem item = new LocalizedMenuItem();

			item.setAction( menuAction );

			return item;
		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param name DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public JMenu getMenu( final String name )
	{
		JMenu menu = new JMenu();

		menu.setText( getText( name, MENU_TEXT_KEY ) );

		KeyStroke key = getKeyStroke( name, MNEMONIC_KEY );

		if( key != null )
		{
			menu.setMnemonic( key.getKeyCode() );
		}

		return menu;
	}

	/**
	 * Method createMenubar.
	 *
	 * @param f Frame in which to set the menu bar.
	 */
	public void setMenubar( JFrame f )
	{
		String menus = resources.getString( "menubar" ); //$NON-NLS-1$

		if( menus != null )
		{
			JMenuBar bar = new JMenuBar();

			StringTokenizer tok = new StringTokenizer( menus, ";" ); //$NON-NLS-1$

			while( tok.hasMoreTokens() )
			{
				String s = tok.nextToken().trim();

				JMenu menu = getMenu( s );

				fillMenu( menu, s );

				bar.add( menu );
			}

			f.setJMenuBar( bar );
		}
	}

	/**
	 * Method fillMenu.
	 *
	 * @param menu Menu to add the contents to
	 * @param menuName Name used as key to determine the contents.
	 */
	private void fillMenu( JMenu menu, String menuName )
	{
		String content = resources.getString( menuName + ".contents" ); //$NON-NLS-1$

		if( content != null )
		{
			StringTokenizer tok = new StringTokenizer( content, ";" ); //$NON-NLS-1$

			while( tok.hasMoreTokens() )
			{
				String s = tok.nextToken().trim();

				if( "-".equals( s ) ) //$NON-NLS-1$
				{
					menu.addSeparator();
				}
				else if( s.startsWith( "menu." ) ) //$NON-NLS-1$
				{
					JMenu subMenu = getMenu( s );

					fillMenu( subMenu, s );

					menu.add( subMenu );
				}
				else
				{
					menu.add( getMenuItem( s ) );
				}
			}
		}
	}
}
