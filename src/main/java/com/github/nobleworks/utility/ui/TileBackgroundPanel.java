package com.github.nobleworks.utility.ui;

import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;

import javax.swing.Icon;
import javax.swing.JPanel;


/**
 * A JPanel subclass that fills its background by tiling an icon.
 * 
 * @author Dale King, Nobleworks Software
 */
@SuppressWarnings("serial")
public class TileBackgroundPanel extends JPanel
{
	/** The icon to be tiled as a background for the panel. */
	private Icon tileIcon = null;

	/**
	 * Create a new TileBackgroundPanel with a double buffer and a flow layout.
	 */
	public TileBackgroundPanel()
	{
		super();
	}

	/**
	 * Create a new buffered TileBackgroundPanel with the specified layout
	 * manager.
	 *
	 * @param layout  the LayoutManager to use
	 */
	public TileBackgroundPanel( LayoutManager layout )
	{
		super( layout );
	}

	/**
	 * Creates a new TileBackgroundPanel with the specified layout manager and
	 * buffering strategy.
	 *
	 * @param layout  the LayoutManager to use
	 * @param isDoubleBuffered  a boolean, true for double-buffering, which
	 *        uses additional memory space to achieve fast, flicker-free
	 *        updates
	 */
	public TileBackgroundPanel( LayoutManager layout, boolean isDoubleBuffered )
	{
		super( layout, isDoubleBuffered );
	}

	/**
	 * Create a new TileBackgroundPanel with FlowLayout and the specified
	 * buffering strategy. If <code>isDoubleBuffered</code> is true, the
	 * TileBackgroundPanel will use a double buffer.
	 *
	 * @param isDoubleBuffered  a boolean, true for double-buffering, which
	 *        uses additional memory space to achieve fast, flicker-free
	 *        updates
	 */
	public TileBackgroundPanel( boolean isDoubleBuffered )
	{
		super( isDoubleBuffered );
	}

	/**
	 * Gets the current icon that is used as a background.
	 *
	 * @return The current Icon used for the background. A value of null means
	 *         no background is drawn.
	 */
	public Icon getIcon()
	{
		return tileIcon;
	}

	/**
	 * Sets the icon to use for tiling as the background of the panel, which
	 * forces the panel to be repainted.
	 *
	 * @param newIcon the Icon to tile in the background. A value of null
	 *        causes nothing to be drawn as the background.
	 */
	public void setIcon( Icon newIcon )
	{
		tileIcon = newIcon;
		repaint();
	}

	/**
	 * Paints the panel. Calls the super class' paintComponent method and if
	 * there is a tileIcon set with a positive height and width it is drawn in
	 * the background inside of the insets.
	 *
	 * @param g the Graphics object to draw into.
	 */
	@Override
	protected void paintComponent( Graphics g )
	{
		super.paintComponent( g );

		// load the icon into a local variable in case someone
		// changes it while we are painting.
		Icon icon = tileIcon;

		if( icon != null )
		{
			int w = icon.getIconWidth();
			int h = icon.getIconHeight();

			if( ( w > 0 ) && ( h > 0 ) )
			{
				Insets inset = getInsets();

				int width = getWidth() - inset.left - inset.right;
				int height = getHeight() - inset.top - inset.bottom;

				Graphics cg = g.create( inset.left, inset.top, width, height );

				for( int y = 0; y < height; y += h )
				{
					for( int x = 0; x < width; x += w )
					{
						icon.paintIcon( this, cg, x, y );
					}
				}

				cg.dispose();
			}
		}
	}
}
