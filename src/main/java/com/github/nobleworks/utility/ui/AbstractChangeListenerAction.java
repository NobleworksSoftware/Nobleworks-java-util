package com.github.nobleworks.utility.ui;

import javax.swing.AbstractAction;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Dale King, Nobleworks Software
 */
@SuppressWarnings("serial")
public abstract class AbstractChangeListenerAction extends AbstractAction
implements ChangeListener
{
	public AbstractChangeListenerAction( String name )
	{
		super( name );
	}

	public void stateChanged( ChangeEvent evt )
	{
		// Do nothing by default
	}
}
