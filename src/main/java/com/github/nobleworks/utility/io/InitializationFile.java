package com.github.nobleworks.utility.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.nobleworks.util.Messages;


/**
 * Class for loading a Windows-style initialization (.ini) file.
 * 
 * @author Dale King, Nobleworks Software
 */
public class InitializationFile implements Iterable< InitializationFile.Section >
{
	/**
	 * Regular expression pattern for lines that should be ignored. The
	 * regular expression includes lines that are entirely whitespace
	 * or those whose first non-whitespace character is a semicolon
	 * which are comments.
	 */
	private static Pattern ignorePattern =
			Pattern.compile( "^\\s*(;.*)?$" ); //$NON-NLS-1$

	/**
	 * Regular expression pattern for lines that declare a section name.
	 * The regular expression includes lines that have text surrounded by
	 * [] and possibly whitespace.
	 */
	private static Pattern sectionPattern =
			Pattern.compile( "^\\s*\\[(.*)\\]\\s*$" ); //$NON-NLS-1$

	/**
	 * Regular expression pattern for lines that declare a key and
	 * value.
	 */
	private static Pattern propertyPattern =
			Pattern.compile( "^\\s*(\\S.*)\\s*=\\s*(\\S.*)\\s*$" ); //$NON-NLS-1$

	/**
	 * The list of sections in this file.
	 */
	private List< Section > sections = new ArrayList< Section >();

	/**
	 * Object representing one section of the initialization file.
	 * @author Dale King
	 */
	public static class Section implements Iterable< Map.Entry< String, String > >
	{
		/**
		 * The name of the section.
		 */
		private final String name;

		/**
		 * The key-value pairs for this section.
		 */
		private final Map< String, String > properties = new LinkedHashMap< String, String >();

		/**
		 * Constructor for a section.
		 * @param name The name for the section.
		 */
		public Section( String name )
		{
			this.name = name;
		}

		/**
		 * Gets the name of the section.
		 * @return The name of the section
		 */
		public String getName()
		{
			return this.name;
		}

		/**
		 * Returns an iterator for the key-value pairs in this section.
		 * @return An iterator for the key-value pairs.
		 * @see java.lang.Iterable#iterator()
		 */
		public Iterator< Map.Entry< String, String > > iterator()
		{
			return properties.entrySet().iterator();
		}

		/**
		 * Add the given property to this section.
		 *
		 * @param propertyName The name of the property to set.
		 * @param propertyValue The value to set the property to.
		 * @return The previous value of the property or null if the property
		 *     was not set.
		 * @throws NullPointerException if name or value is null.
		 */
		public String addProperty( String propertyName, String propertyValue )
				throws NullPointerException
				{
			if( propertyName == null )
			{
				String message = Messages.getString( "NullNameMessage" ); //$NON-NLS-1$
				throw new NullPointerException( message );
			}
			else if( propertyValue == null )
			{
				String message = Messages.getString( "NullValueMessage" ); //$NON-NLS-1$
				throw new NullPointerException( message );
			}
			else
			{
				return properties.put( propertyName.toLowerCase(), propertyValue );
			}
				}

		/**
		 * Get the value for the given property.
		 *
		 * @param propertyName The name of the property to query.
		 * @return The value of the property or null if the property
		 *     is not set.
		 */
		public String getProperty( String propertyName )
		{
			return properties.get( propertyName.toLowerCase() );
		}
	}

	/**
	 * Returns an iterator for the sections in this file.
	 * @return An iterator for the sections.
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator< Section > iterator()
	{
		return Collections.unmodifiableList( sections ).iterator();
	}

	/**
	 * Constructor that reads an initialization file from the given
	 * <code>Reader</code>.
	 * @param in The input to read the initialization file from.
	 * @throws IOException If there is an error reading.
	 * @throws ParseException If the input does not conform to the
	 * initialization file format.
	 */
	public InitializationFile( Reader in ) throws IOException, ParseException
	{
		BufferedReader r = new BufferedReader( in );
		Section currentSection = null;

		for( String s = r.readLine(); s != null; s = r.readLine() )
		{
			Matcher propertyMatcher = propertyPattern.matcher( s );
			if( propertyMatcher.matches() )
			{
				if( sections.isEmpty() )
				{
					currentSection = new Section( null );
					sections.add( currentSection );
				}

				String name = propertyMatcher.group( 1 );
				String value = propertyMatcher.group( 2 );

				currentSection.addProperty( name, value );
			}
			else
			{
				Matcher sectionMatcher = sectionPattern.matcher( s );
				if( sectionMatcher.matches() )
				{
					String name = sectionMatcher.group( 1 );
					currentSection = new Section( name );
					sections.add( currentSection );
				}
				else if( !ignorePattern.matcher( s ).matches() )
				{
					String message = Messages.getString(
							"IllegalLineMessage", s ); //$NON-NLS-1$

							throw new ParseException( message, 0 );
				}
			}
		}
	}

	/**
	 * Write out the contents of this initialization file.
	 * @param out Where to write the initialization file.
	 * @throws IOException If unable to write.
	 */
	public void write( PrintWriter out ) throws IOException
	{
		for( Section s : sections )
		{
			out.format( "[%s]%n", s.getName() );

			for( Map.Entry< String, String > entry : s )
			{
				out.format( "%s=%s%n", entry.getKey(), entry.getValue() );
			}
		}
	}
}
