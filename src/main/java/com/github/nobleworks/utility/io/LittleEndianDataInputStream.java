package com.github.nobleworks.utility.io;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * A little-endian counterpart to <code>java.io.DataInputStream</code>.
 * 
 * @author Dale King, Nobleworks Software
 * @see java.io.DataInputStream
 */
public class LittleEndianDataInputStream extends FilterInputStream
{
	/**
	 * Create a little-endian DataInputStream.
	 *
	 * @param in The raw InputStream to wrap.
	 */
	public LittleEndianDataInputStream( InputStream in )
	{
		super( in );
	}

	/**
	 * Reads a <code>boolean</code> from this data input stream. This method
	 * reads a single byte from the underlying input stream. A value of
	 * <code>0</code> represents <code>false</code>. Any other value
	 * represents <code>true</code>. This method blocks until either the byte
	 * is read, the end of the stream is detected, or an exception is thrown.
	 *
	 * @return the <code>boolean</code> value read.
	 *
	 * @exception IOException   if an I/O error occurs.
	 *
	 * @see java.io.FilterInputStream#in
	 */
	public final boolean readBoolean() throws IOException
	{
		int ch = in.read();

		if( ch < 0 )
		{
			throw new EOFException();
		}

		return ch != 0;
	}

	/**
	 * Reads a signed 8-bit value from this data input stream. This method
	 * reads a byte from the underlying input stream.
	 *
	 * <p>
	 * This method blocks until either the byte is read, the end of the stream
	 * is detected, or an exception is thrown.
	 * </p>
	 *
	 * @return the next byte of this input stream as a signed 8-bit
	 *         <code>byte</code>.
	 *
	 * @exception IOException If an I/O error occurs.
	 *
	 * @see java.io.FilterInputStream#in
	 */
	public final byte readByte() throws IOException
	{
		return (byte) readUnsignedByte();
	}

	/**
	 * Reads <code>b.length</code> bytes from this data input stream into the
	 * byte array. This method reads repeatedly from the underlying stream
	 * until all the bytes are read. This method blocks until all the bytes
	 * are read, the end of the stream is detected, or an exception is
	 * thrown.
	 *
	 * @param b   the buffer into which the data is read.
	 *
	 * @exception EOFException  if this input stream reaches the end before
	 *            reading all the bytes.
	 * @exception IOException   if an I/O error occurs.
	 *
	 * @see java.io.FilterInputStream#in
	 */
	public final void readFully( byte[] b ) throws IOException
	{
		readFully( b, 0, b.length );
	}

	/**
	 * Reads exactly <code>len</code> bytes from this data input stream into
	 * the byte array. This method reads repeatedly from the underlying
	 * stream until all the bytes are read. This method blocks until all the
	 * bytes are read, the end of the stream is detected, or an exception is
	 * thrown.
	 *
	 * @param b     the buffer into which the data is read.
	 * @param off   the start offset of the data.
	 * @param len   the number of bytes to read.
	 *
	 * @exception EOFException  if this input stream reaches the end before
	 *            reading all the bytes.
	 * @exception IOException   if an I/O error occurs.
	 *
	 * @see java.io.FilterInputStream#in
	 */
	public final void readFully( byte[] b, int off, int len ) throws IOException
	{
		int n = 0;

		while( n < len )
		{
			int count = in.read( b, off + n, len - n );

			if( count < 0 )
			{
				throw new EOFException();
			}

			n += count;
		}
	}

	/**
	 * Reads four input bytes and returns an <code>int</code> value. Let
	 * <code>a</code> be the first byte read, <code>b</code> be the second
	 * byte, <code>c</code> be the third byte, and <code>d</code> be the
	 * fourth byte. The value returned is:
	 *
	 * <p>
	 * <pre>
	 * <code>
	 * (((d &amp; 0xff) &lt;&lt; 24) | ((c &amp; 0xff) &lt;&lt; 16) |
	 * &#32;((b &amp; 0xff) &lt;&lt; 8) | (a &amp; 0xff))
	 * </code></pre>
	 * This method is suitable for reading bytes written by the
	 * <code>writeInt</code> method of <code>LittleEndianDataOutput</code>.
	 * </p>
	 *
	 * @return the <code>int</code> value read.
	 *
	 * @exception EOFException  if this stream reaches the end before reading
	 *            all the bytes.
	 * @exception IOException   if an I/O error occurs.
	 */
	public final int readInt() throws IOException
	{
		int w1 = readUnsignedShort();
		int w2 = readUnsignedShort();

		return (w2 << Short.SIZE) | w1;
	}

	/**
	 * Reads two input bytes and returns a <code>short</code> value. Let
	 * <code>a</code> be the first byte read and <code>b</code> be the second
	 * byte. The value returned is:
	 *
	 * <p>
	 * <pre><code>(short)((b &lt;&lt; 8) * | (a &amp; 0xff))
	 * </code></pre>
	 * This method is suitable for reading the bytes written by the
	 * <code>writeShort</code> method of
	 * <code>LittleEndianDataOutputStream</code>.
	 * </p>
	 *
	 * @return the 16-bit value read.
	 *
	 * @exception EOFException  if this stream reaches the end before reading
	 *            all the bytes.
	 * @exception IOException   if an I/O error occurs.
	 */
	public final short readShort() throws IOException
	{
		return (short) readUnsignedShort();
	}

	/**
	 * Reads an unsigned 8-bit number from this data input stream. This method
	 * reads a byte from this data input stream's underlying input stream and
	 * returns that byte. This method blocks until the byte is read, the end
	 * of the stream is detected, or an exception is thrown.
	 *
	 * @return the next byte of this input stream, interpreted as an unsigned
	 *         8-bit number.
	 *
	 * @exception EOFException  if this input stream has reached the end.
	 * @exception IOException   if an I/O error occurs.
	 *
	 * @see java.io.FilterInputStream#in
	 */
	public final int readUnsignedByte() throws IOException
	{
		int ch = in.read();

		if( ch < 0 )
		{
			throw new EOFException();
		}
		else
		{
			return ch;
		}
	}

	/**
	 * Reads two input bytes and returns an <code>int</code> value in the range
	 * <code>0</code> through <code>65535</code>. Let <code>a</code> be the
	 * first byte read and <code>b</code> be the second byte. The value
	 * returned is:
	 *
	 * <p>
	 * <pre><code>(((b &amp; 0xff) &lt;&lt; 8) | (a &amp; 0xff))
	 * </code></pre>
	 * This method is suitable for reading the bytes written by the
	 * <code>writeShort</code> method of
	 * <code>LittleEndianDataOutputStream</code>  if the argument to
	 * <code>writeShort</code> was intended to be a value in the range
	 * <code>0</code> through <code>65535</code>.
	 * </p>
	 *
	 * @return the unsigned 16-bit value read.
	 *
	 * @exception EOFException  if this stream reaches the end before reading
	 *            all the bytes.
	 * @exception IOException   if an I/O error occurs.
	 */
	public final int readUnsignedShort() throws IOException
	{
		int value = in.read();
		value |= in.read() << Byte.SIZE;

		// This will evaluate to true if either character returned
		// a negative value indicating EOF
		if( value < 0 )
		{
			throw new EOFException();
		}
		else
		{
			return value;
		}
	}
}

