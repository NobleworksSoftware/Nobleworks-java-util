package com.github.nobleworks.utility.io;

import static com.github.nobleworks.util.Messages.getString;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;


/**
 * A little-endian counterpart to <code>java.io.DataOutputStream</code>.
 * 
 * @author Dale King, Nobleworks Software
 * @see java.io.DataOutputStream
 */
public class LittleEndianDataOutputStream extends FilterOutputStream
{
	/**
	 * A mask for the significant bits in a byte.
	 */
	private static final int BYTE_MASK = 0xFF;

	/**
	 * Create a little-endian DataOutputStream.
	 *
	 * @param out The raw OutputStream to wrap.
	 */
	public LittleEndianDataOutputStream( final OutputStream out )
	{
		super( out );

		if( out == null )
		{
			final String message = getString( "OutNull" ); //$NON-NLS-1$
			throw new NullPointerException(message);
		}
	}

	/**
	 * Writes a <code>boolean</code> to the underlying output stream as  a
	 * 1-byte value. The value <code>true</code> is written out as the  value
	 * <code>(byte)1</code>; the value <code>false</code> is  written out as
	 * the value <code>(byte)0</code>.
	 *
	 * @param booleanToWrite   a <code>boolean</code> value to be written.
	 *
	 * @exception IOException  if an I/O error occurs.
	 */
	public final void writeBoolean( final boolean booleanToWrite ) throws IOException
	{
		out.write(booleanToWrite ? 1 : 0);
	}

	/**
	 * Writes out a <code>byte</code> to the underlying output stream as  a
	 * 1-byte value.
	 *
	 * @param byteToWrite   a <code>byte</code> value to be written.
	 *
	 * @exception IOException  if an I/O error occurs.
	 */
	public final void writeByte( final int byteToWrite ) throws IOException
	{
		out.write( byteToWrite );
	}

	/**
	 * Writes an <code>int</code> to the underlying output stream as four
	 * bytes, low byte first. If no exception is thrown, the counter
	 * <code>written</code> is incremented by <code>4</code>.
	 *
	 * @param v   an <code>int</code> to be written.
	 *
	 * @exception IOException  if an I/O error occurs.
	 */
	public final void writeInt( int v ) throws IOException
	{
		writeShort(v);
		writeShort(v >> Short.SIZE);
	}

	/**
	 * Writes a <code>short</code> to the underlying output stream as two
	 * bytes, low byte first. If no exception is thrown, the counter
	 * <code>written</code> is incremented by <code>2</code>.
	 *
	 * @param v   a <code>short</code> to be written.
	 *
	 * @exception IOException  if an I/O error occurs.
	 */
	public final void writeShort( int v ) throws IOException
	{
		out.write(v & BYTE_MASK);
		out.write((v >>> Byte.SIZE) & BYTE_MASK);
	}
}

