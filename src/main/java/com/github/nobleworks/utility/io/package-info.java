/**
 * Some general purpose I/O related utility classes.
 * @author DaleKing, Nobleworks Software
 */
package com.github.nobleworks.utility.io;